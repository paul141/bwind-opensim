// ======= Header ==============
// BWind_GENOA_1.40-OS-Mesh2
// MODIFIED Opensim/SL mesh support enhanced
//
// Edison Rex - Mesh support and enhancements October 2018
//------------------------------------------------------
// This is a new script that doesn't work like the others and is quite different.
// so that header doesn't apply.
//
// a genoa is a sheet that is attached either port or starboard, kind of like a jib but you unfurl it in place.
// it's only used close to the wind. You don't use it broadside or on reach.
// so this genoa will have basically two positions, either side of the boat depending on the wind angle.
// and you furl and unfurl it instead of moving it. but not too much.
//
// Edison Rex November 2018

//DEFINE VARIABLES
rotation childRot;
rotation newRot;    // sense rotation change to initiate which side to fill
rotation initRot;   // initial rotation of this sail
vector initPos;     // initial position of this sail
rotation ourRot;    // the current rotation of this sail
vector ourPos;      // the current position of this sail
rotation nextRot;   // where to rotate to next
vector pivot = <-3.7,0.0,0.0>;   // typically 1/2 the width, either + or - on X axis, but may be some other hinge point
vector thisPivot;   // the position whereby the centre is offset properly so the angle (hinge) shows up at the mast
vector initPivot;   // our pivot right now (when calculating the next move)
vector rps;         // we only do one movement at a time, this is "rotation per step" and is a radian

float sailAngle;    // the current offset of the sail used to determine what side of the middle we are on
float medAngle;     // median angle - the angle at which the sail is deflected 0 degrees. sailAngle is either > or < this
integer flapping;   // boolean, is the sail flapping. Probably serves no real purpose. FIXME:
integer sailUp;     // is the sail up, to make sure we don't flap or do anything dumb when the sail isn't up


lower() {   // LOWER SAIL ROUTINE - SETS THE SAIL TO ALPHA (CLEAR)
    llSetAlpha(0.0,ALL_SIDES);      // just make the sails invisible right now
    // llSetLinkPrimitiveParamsFast(LINK_THIS, [PRIM_ROT_LOCAL, initRot, PRIM_POS_LOCAL, initPos]);            // must set rotation and position
    medAngle = initRot.y;           // the current deflection is zero deflection but is some relative angle
    sailAngle = medAngle;           // sail current angle is the same as median right now
    sailUp = FALSE;
}

raise () {  // RAISE SAIL ROUTINE
    llSetAlpha(0.0,ALL_SIDES);  // no, don't expose the faces here, we don't know which face to expose yet
    sailUp = TRUE;  
}

reset() { //DEFINE INITIAL SAIL ROTATION
    llSetLinkPrimitiveParamsFast(LINK_THIS, [PRIM_ROT_LOCAL, initRot, PRIM_POS_LOCAL, initPos]);    // must now set both rotation and position  
    initRot = llGetLocalRot();  // reset these
    initPos = llGetLocalPos();
    medAngle = initPos.y; 
    sailAngle = medAngle;  
}

// flap the sails. If you have 4 faces, then set the left side to 2 of them and the right side to the other 2. 
// if you only have 2 faces, set both left and right to both, but reverse the order.

flap(integer side)
{
    flapping = TRUE;
    integer i;
    integer j;
    integer face1;
    integer face2;
    
    if (side > 0)       // side is either +1 or -1 for right or left respectively
    {
        face1 = 1;      // MODIFY THESE FOR YOUR MESH FACES
        face2 = 0;
    }
    else
    {
        face1 = 0;
        face2 = 1;
    }
    //flap the sails, just toggle the alphas a bit, end up on the face you started with
    
    for (i=0;i<5;i++)
        {
           if (j==1)
           {
                llSetAlpha(1.0,face1);      // starboard
                llSetAlpha(0.0,face2);
                j = 0;
               
            }
            else
            {
                llSetAlpha(0.0,face1);
                llSetAlpha(1.0,face2);
                j = 1;
              
            }
            llSleep(.15);
        }
        flapping = FALSE;
       
}

// MAIN SCRIPT ROUTINE
default {
    // most of this entry code is original. We have to add the parts where we care about position.
    state_entry() {
        llSetObjectName("jib");    //SET HERE SAIL PRIM NAME - IMPORTANT ! EACH SAIL MUST HAVE AN UNIQUE NAME TO WORK !
        //initRot=llGetLocalRot();    //INITIAL SAIL ROTATION (ON Z AXIS) DEFINED BY USER
        initRot=llEuler2Rot(<0.0*DEG_TO_RAD,0*DEG_TO_RAD,90*DEG_TO_RAD>); // INSERT HERE THE ANGLE OF JIB ROTATION EX. -31)
        initPos=llGetLocalPos();    // initial sail position because we need it now
        
        llSetLinkPrimitiveParamsFast(LINK_THIS, [PRIM_ROT_LOCAL, initRot, PRIM_POS_LOCAL, initPos]); // must now set both rotation and position 
        medAngle = initPos.y;       // median angle is set to the y rotation at init
        llSetStatus(STATUS_PHANTOM,FALSE);  //SET SAIL SOLID (not physical)    
    }
    
    on_rez(integer param) {
        llAllowInventoryDrop(TRUE); //allows texture dropping on the Sail
    }
    
    //ROOTPRIM COMMANDS LISTENER
    link_message(integer sender,integer num,string str,key id) {
        //llOwnerSay((string)num);
        if (num==1000) {        //reset - CHILD SAIL WILL BE SET CLEAR AND ROTATED TO INITAL POSITION
            lower();
            reset();
            llStopSound();
        }
        else if (num==1001) {   //lower without reset - CHILD SAIL WILL BE SET CLEAR ONLY
            lower();
        }
        else if (num==1002) {   //raise sail CHILD SAIL WILL BE SET NON ALPHA ONLY
            raise();
        }
        else if (num>1999) {    //change variable alpha setting (future use) - FROM ROOTPRIM (this code is useless)
            float alpha=(float)num;
            alpha-=2000;
            alpha/=100.;
            //llSetAlpha(alpha,0); //NOTE : if you want to set your sail to a full transparency on all side use this command :  llSetAlpha(0,ALL_SIDES);
        }  
        else if(num==1010)      // flap the sails
        {
            if (sailUp)
            {
            if (!flapping)
            {
                if(str=="no sound")
                {
                    llStopSound();
                }
                if(str=="flap")
                {
                    llStopSound();
                    llPlaySound("SailingSailFlappingB",1.0); // your flapping sail sound here
                    if (sailAngle > medAngle)
                    {
                        flap(-1);
                    }
                    else
                    {
                        flap(1);
                    }   
                }
                if(str=="green")
                {
                    llStopSound();
                }
            }
            }
        }
        else {                  //rotate sail, CCW is positive
            // this is the old code that did rotation.
            //childRot=llGetLocalRot();
            //eulerRot=<0,0,num*DEG_TO_RAD>;
            //quatRot=llEuler2Rot(eulerRot);
            //llSetLocalRot(quatRot*childRot); // HERE THE SAIL RECEIVES THE ROTATION COMMAND WHILE TRIMMING
            //
            // this now moves the sail

            ourPos = llGetLocalPos();                       // right now where am I
            ourRot = llGetLocalRot();                       // right now what's my rotation

            initPivot = ourPos + pivot * ourRot;            // currently the pivot (offset of the centre of the prim) 
            rps = <0.0,0.0,(float)num * DEG_TO_RAD>;        // going to rotate these degrees, convert to radians
            nextRot = llEuler2Rot(rps) * ourRot;            // next rotation is how much more or less than our current rotation
            
            thisPivot = (initPivot - (pivot * nextRot));    // calculate the pivot based on our pos + (pivot * rotation)
            
            llSetLinkPrimitiveParamsFast(LINK_THIS, [PRIM_ROT_LOCAL, nextRot, PRIM_POS_LOCAL, thisPivot]);      // move the sail     
            
            // now we determine what side of the sail to fill. 
            //ourRot=llGetLocalRot();        
            sailAngle = thisPivot.y;
             
          //llOwnerSay((string)sailAngle+"   "+(string)medAngle);
            if (sailAngle < medAngle)          // are we to the left of the median?
            {
                llSetAlpha(0.0,0);
                llSetAlpha(1.0,1);      // port
                //llSetAlpha(0.0,2);
                //llSetAlpha(0.0,3);      
                //llSetAlpha(0.0,4);
            }
            else
            {
                if (sailAngle > medAngle)      // are we to the right of the median?
                {
                    llSetAlpha(1.0,0);
                    llSetAlpha(0.0,1);
                    //llSetAlpha(1.0,2);
                    //llSetAlpha(0.0,3);
                    //llSetAlpha(0.0,4);               
                }
                else
                {
                    llSetAlpha(0.0,0);      // if it is idle, kill the sails. We should never get here.
                    llSetAlpha(0.0,1);
                   // llSetAlpha(0.0,2);
                   // llSetAlpha(0.0,3);
                   // llSetAlpha(0.0,4); 
                }
                
            }          
            
            }     
    }    
}

// ------------------------------------------ END SAIL SCRIPT ----------------