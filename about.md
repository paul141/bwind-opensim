Bwind 

Bwind has a particularly strange history and many people malign it, but it gets used a lot, more than most people realise. 

There's a lot of credit that should go to Becca Moulliez, although Kanker Greenacre really deserves credit for the basic sail engine.
Balduin Abaye seemed to have a lot of bugfix credits in SL. I lost count of how many contributors there are and doubtless a number are in Opensim.

The journey from SL to Opensim opened a number of options for extension, especially with ubODE physics being so much in need of good scripts. Whilst it can be argued forever whether or not bwind is a good script or not, the changes I've made to it bring considerable stability and some much needed new features to it. Since it's GPL it's still free, free to use, sell extend, or whatever. 

The version numbers since 1-37 in Opensim are arbitrary and are mine. You want to fork it, go ahead. You want to work with me, contact me. Regardless of whether you do or not, I will continue this branch of BWind for use in Opensim.

Either way, this whole package is completely free and open source. I leave Becca's original headers in despite the very clear conflict of intent (declaring something GPL and then restricting it to requiring permission isn't in keeping with the GPL or copyleft). This is very clearly GPL code.

Please make sure you read the BBK_CHANGES notecard so you understand all that's changed. There have been an awful lot of changes and old sail scripts will NOT WORK WITH THIS VERSION. You absolutely must use the new scripts or you need to modify your old scripts to handle the message 1010, or else get used to everything spinning in circles. You were warned.

UNPACKING

The script sets are in boxes inside this box. When you rezz the main scripts box or the sails box, if the box disappears just look for it as it might set its alpha to 0. I didn't disable the scripts because some people don't realise they've been disabled and then try to use them and they don't work. 

NOTES ON MESH SAILS

As of 1-40 you don't need a "counterweight" in your sail to pull the center of the sail to the edge. You just need to specify the offset of where the sail should pivot in the sail code. Same for the boom, jib, or genoa. 

Mesh sails need at least 2 faces. Having more is great, 4 being great for flap effect (2 each side). The scriptset as of 1-40 doesn't handle reef or furled faces, but this is really not hard to implement, just I haven't seen any sails for free that have those faces. 

NOTES ON HANDLING

Bwind in Opensim is really set up for large oceans, of which I am aware of a number of them. ubODE is far better than Bullet for sailing and you should use that for your oceans (if you don't have an ocean check out the Zetaworlds one, and stop by and say hi to me when you do). If you haven't got one, pick up Ocean Engineering's wind setter buoy or check out my WeatherOS info, which is compatible with the OE system but uses real life weather and doesn't require you to hand set every buoy.  For long journeys, the hold mode is really helpful as drift is constant and journeys can be long.

Adding to your boat:

As usual, the main script and pose script go in your root prim. If the boat isn't properly oriented to <0,0,0> as a build you need to build a keel as root prim to orient that way. Put it at or close to the waterline. Add the poses and sailing sound.

AVsitter works amazingly well with bwind, and you can forego the pose script if you decide to use AVsitter.

Sails each need their respective script as does the boom. The pivot for a normally built mesh piece will be + or - 1/2 the X length. Look near the top of the sail/boom script for where to set that. The jib script still needs the tilt angle done as well, as in previous versions. If you are using a prim sail and it's done the old way,  set the pivot at <0.0001,0.0001,0.0001>. Modify the scripts to identify the faces for flap and windside. 

The presence of the following objects on your boat will be noted by the scriptset and have some effect:

1) Fenders. If you have fenders (preferably a single mesh prim or a mesh prim face) name it "fenders" and bwind will make them disappear when sailing and appear when docked.

2) Hatches - see the notes, there's an upper and lower hatch.

3) Sheet - the main boom tackle (sheet) typically needs to disappear when sailing otherwise it looks silly. Someday I'll do a particle rope for this or make a script to stretch it.

4) instruments - see the accessories list! 

NEED HELP?

Got questions, want something, or think there's a bug? Let me know.  I generally won't ask for money for this work but any work I do will go back into the release and will remain free. I can't stop you from selling this, but I can do my best to show people that it is free anyway. 

Edison Rex
November 2018

