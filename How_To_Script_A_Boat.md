    how to script a boat for yourself!
    
    if you've built or found or otherwise got a sailboat and it's not scripted yet, and you want to script with bwind (because it's free or because it's good) and you have no idea how to get started, well here's directions for you. I'm going to break this down into particular components since that's how the scripts are arranged and how you need to put the boat together. If you follow these instructions you can have a boat seaworthy in under an hour (I'm able to get the basics done in about 10 minutes, it's the details that matter).

1) THE HULL

set your hull to face east. Edit and open the object tab and look at the orientation. x,y,z should be <0,0,0> 
If they aren't, the boat is going to need a keel as the root prim. Or you need to fix the mesh to be <0,0,0> rotation facing east.
The actual hull should be your root prim, or the keel. The keel, if it is the root prim, should be near where you want the waterline. A little lower is fine.
Drag the main bwind script (currently the BWIND-SAIL-ENGINE-1-40-OS-MESHC) it's always the sail engine, put that in the root prim. Add a sound called "Sailing" in, there's one in the kit or you can get your own (I use Audacity to edit sounds I capture in RL or download from places like freesound.org)

Poses are not part of bwind's main script. You will choose a simple pose (the bwind pose script) or something more complex (avsitter) later.
NOTA BENE:::: I do not recommend PMAC for boats. Mostly because lots of sims won't give you permission for the OSSL calls it uses. If you do get PMAC working with boats, remember it has to be in the root prim too. AVsitter doesn't need to be in the root prim but it makes it easier to edit when you put the main bits in. 

To make the boat sit right, you probably will have to make some adjustments to the script variables. By "Sit right" I mean it needs to sit at the waterline when you sit on the boat. You can place the boat anywhere you want but when it goes physical when you sit on it, the script will set the float point.

This is done at line 603:

    //if over water, set boat height to sealevel + 0.1m; this is the standard default water level
    if (groundHeight <= waterHeight) {
        pos.z = waterHeight + 0.5; 
        while (llVecDist(llGetPos(),pos)>.001) llSetPos(pos);
    }
}

+ 0.5 means the boat was sitting too low and we raised it over the waterline. You can make it " - 0.5" to lower it 1/2 meter, or some number.
This is where you do that though.

When I refer to "the sail engine" I'm referring to this main script. This is important as you add parts to your boat.

Now the boat will float, and sail if you sit on it. You can test this by right clicking and choosing "Sail!" .

The boat should float near the waterline, and if you type "raise" it should go into sailing mode and you can sail around. Mind you with no sails or wake it looks like crap, and you'll be sitting in the middle of the boat's keel, but it should be working.

If the boat turns on its side or you sail in some direction not forwards, you need to go right back to the beginning and build a keel.

2) MAINSAIL AND BOOM

For the boom I'm not going to make much of a distinction between mesh and prim. The new mesh support boom code can handle a prim center that is not at the edge, but if the prim center has been moved, it can handle that too.

First, name the prim "boom" 
Second, drag in the boom script.  Current boom script is BBK_BOOM_1-40-OS-MeshC.
At line 21, you set the pivot point. 

vector pivot = <-4.0,0.0,0.0>;   // the offset to pivot from (the hinge point)

you find this point by looking at the prim's length. Typically the X axis is what we will pivot and we will use the Z axis to pivot on. So we're defining a hinge. If the center of the prim shows as the point you want to pivot at, set X to .000001 to avoid divide by zero errors.

Otherwise use 1/2 of the X size. It might be - or + depending on the prim.

You can test it by going for a quick sail. if you type "raise" the boom will move away from the wind direction, and up arrow will let it out a bit.

It should pivot at the mast. If it doesn't, you need to re-examine the pivot point. Making it just slightly larger than the prim size is good because it should pivot from the center of the mast.

Now for the mainsail, this is a little more complicated if you have mesh, but if you just made a prim sail it's very simple.
For a prim sail, as for the boom, set the pivot point. A prim sail cannot flap. Set the constant hasFaces = FALSE if that is the case.
That will shut off the flap function. Otherwise leave it true.

If you have mesh sails and they have multiple faces for the flapping effect, then identify which face is which. There should ideally be two on the right and two on the left. Some mesh sails will have additional faces which were added as "counterweights", identify those as they will always be transparent. They are not necessary to have, so if your sails have a center not at an edge, don't worry, you set the pivot to be the same number as the boom it will sit on. Otherwise if there's a pivot at the edge you set the pivot X to .00001 as with the boom. 

Assume you have 4 faces, 0,1,2,3 which go in order from right to left. You need to set (starting at line 84) the faces for the flap, and later you need to set the faces for the sail sides. 

The script will want to know which faces to expose when the sail is leaning to port or starboard. It can tell  if it is to the right or left of the median.

So you have to tell the script both for the flap, and for the actual expose code.

so at line 88:

    if (side > 0)       // side is either +1 or -1 for right or left respectively
    {
        face1 = 1;      // MODIFY THESE FOR YOUR MESH FACES
        face2 = 2;
    }
    else
    {
        face1 = 4;
        face2 = 3;
    }
    
generally expect side > 0 to mean the sail angle is to the right side, <0 means to the left side.

then down at line 206 and onwards:

            if (sailAngle < medAngle)          // are we to the left of the median?
            {
                llSetAlpha(0.0,0);
                llSetAlpha(0.0,1);      // port
                llSetAlpha(0.0,2);    // if you have other faces they get set here maybe
                llSetAlpha(0.0,3);     
                llSetAlpha(1.0,4);
            }
            else
            {
                if (sailAngle > medAngle)      // are we to the right of the median?
                {
                    llSetAlpha(0.0,0);
                    llSetAlpha(1.0,1);
                    llSetAlpha(0.0,2);
                    llSetAlpha(0.0,3);
                    llSetAlpha(0.0,4);               
                }
                else
                {
                    llSetAlpha(0.0,0);      // if it is idle, kill the sails. We should never get here.
                    llSetAlpha(0.0,1);
                    llSetAlpha(0.0,2);
                    llSetAlpha(0.0,3);
                    llSetAlpha(0.0,4); 
                }
    
    for as many faces as you have, you set which faces to expose depending on the sail angle.
    Always set the counterweight alpha to 0. Always make enough of these so that every prim is accounted for.
    In some cases I have seen a boom face attached to the sail, you can expose it here on both sides.
    
    The jib and spinnaker (and genoa) work exactly the same way. 
    
    For the jib, you also set the pivot angle so it aligns properly. Set the prim where you want it, note the rotation and use that  at line 121:
    
            initRot=llEuler2Rot(<0.0*DEG_TO_RAD,342*DEG_TO_RAD,0*DEG_TO_RAD>); // INSERT HERE THE ANGLE OF JIB ROTATION EX. -31)

2) ACCESSORIES

Some accessories need to be named in order for them to respond correctly. Some of them need to have scripts added.
Here's a handy list and what you need to do.

a) fenders - if you have mesh fenders as a single prim, just name the linked prim "fenders". They will disappear when you raise the sail and reappear when you lower the sail.

b) cover - a prim called cover will do the same. This cover could be the sail cover or a cockpit cover. Whatever.

c) L33 - weird name but it refers to the boom's sheet which is usually a bunch of tackle. It looks dumb when you move the boom. This makes it disappear.

d) hatchlower and hatchupper - if you want a fixed hatch to open when moored and to close when under sail, name them hatchlower and hatchupper.
You can also just script your hatches to open and shut on touch. 

e) windvane - name the windvane prim "windvane" and add the windvane script. You may need to offset the direction by 90 degrees one way or the other. That script explains where to do that.

f) instruments - you can take the bwind instruments available in my chandlery and simple link them to your build and they will work. They come with instructions too, but generally speaking there is a prim name for each and just make sure that doesn't change.

g) wake - link the wake to the bow at the waterline. It's generally set to work for most boats.

3) POSES

for a small boat you can use the bwind pose script. Set the position and add a pose animation. 

For a bigger boat, use avsitter. I'm not teaching that here. But treat the boat like a piece of furniture and make the first sitter the pilot, and you should be fine. There are examples I've done if you want to look.

Fair sailing! If you have questions, try to find me in my home region on Zetaworlds, Pomsigland, or send me an IM. I'm always interested in seeing what people are up to.

Edison Rex, January 2019
