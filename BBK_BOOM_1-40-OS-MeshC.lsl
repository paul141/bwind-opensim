//////////////////////////////////////////////////////
// BWind_Mesh_Boom_Offset 
// BBK_BOOM_1-40-OS-MeshC                        /////
// BWind Boats - BOOM script                     /////
//  Edison Rex (based on Becca Moulliez original /////
//  bwind boom script. This retains the call functions ///
/////////////////////////////////////////////////////
//
// this script will turn mesh objects that have normal centres (i.e. no offset prim)
// it will calculate pivot from a hinge point.
// uses a technique from Lyn Mimistrobell's door script, but not her code.
// the maths are the same anyway. Using this we can make sails or booms with mesh that doesn't have an offset prim.
// i.e we can just define the pivot point for any sail or other thing on the boat

rotation childRot;          // the actual rotation of the child (this prim)
vector childPos;            // the actual position (this should be relative to the root prim)
rotation initRot;           // initial rotation of this prim
vector initPos;             // initial position (relative to the root prim)
vector ourPos;              // the current position
rotation ourRot;            // the current rotation
rotation nextRot;           // where to rotate the child
vector pivot = <-4.0,0.0,0.0>;   // the offset to pivot from (the hinge point)

vector thisPivot;           // the position whereby the centre is offset properly so the angle still hinges        
vector initPivot;           // our pivot right now (when calculating to move)
vector rps;                 // we only do one movement at a time. This is "rotation per step"


lower() {
    llSetLinkPrimitiveParamsFast(LINK_THIS, [PRIM_ROT_LOCAL, initRot, PRIM_POS_LOCAL, initPos]);            // must set rotation and position

    //llSetAlpha(0.0,ALL_SIDES);    // for a sail, maybe this is ok when lowering. Not when raising though. Don't bother at all with a boom.
}

raise () {
    // if this were a sail, when we raise the sail we really just want to set a flag or something and let the sail logic light up which face
    // and you need to know what faces there are so in the sail code we'll do that in the definitions
     //llSetAlpha(1.0,ALL_SIDES);   // in ANY case don't do this statement at all with mesh sails. 
}

reset() {
    llSetLinkPrimitiveParamsFast(LINK_THIS, [PRIM_ROT_LOCAL, initRot, PRIM_POS_LOCAL, initPos]);            // must now set both rotation and position    
}

default {
    
    state_entry() {
        llSetObjectName("boom");              //SET HERE SAIL PRIM NAME
        //initRot=llEuler2Rot(<0,0,PI_BY_TWO>); //MAINSAIL DOES NOT ROTATE FRONTALLY
        initRot=llEuler2Rot(<0,0,0*DEG_TO_RAD>); //BOOM DOES NOT ROTATE FRONTALLY 
        //initRot=llGetLocalRot();                // preserve the rotation we just set in case it has moved a bit
        initPos=llGetLocalPos();                // we need both position and pos. This is important and the sails need it even more.
        llSetLinkPrimitiveParamsFast(LINK_THIS, [PRIM_ROT_LOCAL, initRot, PRIM_POS_LOCAL, initPos]);            // must now set both rotation and position  
        llSetStatus(STATUS_PHANTOM,FALSE);      // shouldn't have to do this but bwind can do some dumb things     
    }
    
    on_rez(integer param) {
        llAllowInventoryDrop(TRUE);             // this is so you can change textures, but you should be able to anyway in Opensim assuming this code stays free which it better do
    }
    
    link_message(integer sender,integer num,string str,key id) {
        if (num==1000) {        //reset
            lower();
            reset();
        }
        else if (num==1001) {   //lower without reset
            lower();
        }
        else if (num==1002) {   //raise sail
            raise();
        }
        else if (num==1010)
        {
            // handle the flap condition but do nothing (it's a boom innit)
        }
        else if (num>1999) {    //change alpha setting (this code is not called anywhere in bwind)
            float alpha=(float)num;
            alpha-=2000;
            alpha/=100.;
            //llSetAlpha(alpha,0); // and if it is called, don't do whatever this is.
        }   
        else {                  //rotate sail, CCW is positive
            // to handle booms that are not offset, or any mesh, let's do some extra maths
            // to pivot from an arbitrary hinge point, we will calculate the rotation from that point,
            // and move the position of the boom to match that rotation as the centre of the boom will change.
    
            ourPos = llGetLocalPos();                       // right now where am I
            ourRot = llGetLocalRot();                       // right now what's my rotation

            initPivot = ourPos + pivot * ourRot;            // currently the pivot (offset of the centre) 
            rps = <0.0,0.0,(float)num * DEG_TO_RAD>;        // going to rotate this much, in radians
            nextRot = llEuler2Rot(rps) * ourRot;           // next rotation is how much more or less than our current rotation
            
            thisPivot = (initPivot - (pivot * nextRot));            // calculate the pivot based on our pos + (pivot * rotation)
            
            llSetLinkPrimitiveParamsFast(LINK_THIS, [PRIM_ROT_LOCAL, nextRot, PRIM_POS_LOCAL, thisPivot]);
        }     
    }    
}