// --------------------------------------------------------
// SAILING SCRIPT VERSION 1-43 Opensim Motor Edition
// --------------------------------------------------------
// BWind Sailing Engine Release 1-43-OS Motor Edition
// (sort of) by Becca Moulliez - (original) RELEASE June 2010 - GNU/GPL
// with many changes along the way, it seems
// SAIL + APPARENT WIND
// refer to boat name for release :
// BOAT RELEASE-WIND ENGINE VERSION-LATEST UPDATE VERSION
// i.e : 1-1-1
// script based upon Kanker Greenacre's Flying Tako
//------------------------------------------------------
//            Please do not remove this header
//                Permitted Free Uses
//  - allowed to use in your personal boats. 
//  - allowed to use in boats you wish to sell or give away in second life.
//  - allowed to modify any part to your particular needs
// ======= End Header =========
//
// All that being said above, how can we be GNU and restricted to SL at the same time? I don't think that works.
// So since this is a substantial modification, the GNU fits. GPL for the win.
//
// SEE THE NOTECARD DISTRIBUTED WITH THIS SCRIPTSET FOR ALL THE CHANGELOGS. THERE ARE FAR TOO MANY TO TAKE UP SPACE HERE WITH.

// ----------------------------------------- START PROGRAMME ---


// GLOBAL BOAT SETTINGS 
// you don't want to modify these settings  unless specifically noted 
/////////////////////////////////////////////////////////////////////
// 1.41 all unused variables have been removed from the engine. 
// 1.43 changes to heel maths to add hull width for cats, also makes the heel more realistic for a monohull too.
// also set the keel height in one place now, so it is consistent. This has been a perennial problem.

//version settings
string boatName="Your Awesome Sailboat";   // for your boat's name. This helps me keep track of variants. 
string versionNumber="1-43-OS-ME";  // Release reference

//environment
vector W_pos=<3000,3000,3000>;    //location of last Wind Setter Buoy -- Kayaker
key   W_id;                 //UUID of last Wind Setter -- Kayaker
float W_dog;                //Watch dog timer, how long since I heard from that buoy -- Kayaker

vector wind;                // the vector representing wind either set or received from setter
float windAngle;            // for apparent wind calc
float absWindAngle;         // expressed as an absolute
float seaLevel;             // sim's sealevel

//reused math variables
vector eulerRot;
vector currEuler;
rotation quatRot;
rotation currRot;

//boat variables
float zRotAngle;            //actual Z rotation angle, i.e where are we pointing towards
vector leftVec;             // used for leeway, etc
float compass;              // our Z rotation kept as a true compass angle

//heeling variables
float heelAngle;            // for heeling and leeway
float heelTorque;
float heelAdd;
float beam=6.0;             // width (Y axis) of the boat, to calculate raising the centre of the hull
                            // so a catamaran won't sink its hulls
                            // for rotation along x-axis just leave as zero
float heelHeight;           // z-axis (height) component of the heel

//linear motion variables
float currSpeed;
vector groundSpeed=ZERO_VECTOR;
float spdFactor=0.0;
float leeway;               // actual leeway

//angular motion variables
float rotSpeed;             // our calculated turning rate

//sail variables
integer sailingAngle;
integer currBoomAngle=0;
integer delta;
integer incr;
float optBoomAngle;             // optimal boom angle (in the green)
float trimFactor;

//spinnaker variables

integer SPIN_UP=FALSE;          // spinnaker up?
integer genoaUp=FALSE;          // genoa up?
integer CurSpin;                // spinnaker working angle
string Tack;                    // literally display tack side

//performance constants - Standard defaults

float timerFreq=1.0;            //timer frequency, seconds (original paramer 1.5; check this if gets laggy)
integer sheetAngle=5;           //initial sheet angle setting
integer trueAngle;
float maxWindSpeed=25.0;        //used for heeling calculation (better leave this unchanged)
integer maxAngle=80;            // set max boom angle here. 

//miscellaneous settings

key owner;                      //boat owner 
key avatar;                     //avatar sitting at the helm
integer lock=FALSE;             // for setter mode
integer ownerVersion=FALSE;     // not very useful way to make the boat public access
integer SAIL_UP=FALSE;          // sails up?
integer motorOn=FALSE;          // motor on?
integer permSet=FALSE;          // did we get our avi permissions granted
integer HUD_ON=TRUE;            // this flag you can set to use the text hud on or off
string idStr;                   // for the rarely used id string setter
integer sailing=TRUE;           // are we in physical mode
float convert=1.944;            // this actually is used to convert (in this case knots)
string units=" Kts.";           // display unit in hud
string helpString;              // displayed help
string visualAid;               // displayed non-coloured indicator
vector hudcolour=<1,1,1>;       // white for start
string currentString;           // not very useful (deprecating) hud storage. 
integer ADV_HUD=TRUE;           //Advanced HUD off by default; set to TRUE to set on...
integer useSetter=FALSE;        // flag to use wind setter buoys
integer courseHold=FALSE;       // flag to indicate course hold is set
float courseHoldValue;          // the course to hold
integer Tacking=FALSE;          // does this boat use the tacking animations? Set to TRUE if so. (leave FALSE if you're using AVSitter)
integer klimit = FALSE;         // keyboard press limit for throttle up and down
integer autoTrim=FALSE;         // auto trim mode
integer autoTack=FALSE;         // auto tack mode
float keelTrim=0.75;             // <--- this sets your height in the water so you don't mess with prims

//linked parts - childprims declarations; the rootscript will send commands to childs when running...
//to work properly relevant prims MUST be renamed following the scheme below

integer JIB;
integer SAIL;
integer BOOM;
integer HUD;
integer CREWMAN;                //this refers to the Crew Poseball in the cockpit... (very obsolete)
integer POLE;                   //Spinnaker
integer SPINNAKER;              //Spinnaker
integer GENOA;                  //genoa sail support
integer FENDERS;                //if boat is equipped with fenders
integer FENDER2;                // if port and starboard are separate
integer WINDVANE;               //support windvane direction
integer HATCHUPPER;
integer HATCHLOWER;             // for closing and opening the hatch
integer L33;                    // boom tackle
integer AWI1;                   // apparent wind gauge
integer TWI1;                   // true wind gauge
integer SAI1;                   // Sail Angle Indicator gauge
integer STI1;                   // Sail Trim Indicator
integer INS1;                   // Instrument Cluster 1


//general sailing parameters          
float ironsAngle=28;            //this is as close as the boat can sail to the wind
float slowingFactor=0.7;        //speed drops by this factor every timerFreq seconds if sailing into the wind
float leewayTweak=1.50;         //scales leeway (0=no leeway)
float rotTweak=0.8;             //scales boat turning rate
float speedTweak=1.0;           //Modifying this will make unrealistic speeds, it still gets modified within the script anyway.
                                // Best not to change this here.
//Apparent Wind parameters
vector tmpwind;                 // to store the wind vector for calculation
float truewindAngle;            // derived TWA
float appwindSpeed;             // derived AWS
float appwindAngle;             // derived AWA now fixed 

// Buoy listener/setter globals
//
integer     windh=-1;                //handle for listening to scripted wind
vector      windvec=<0,1,0>;        //default to north

// motor mode globals
//
float fuelMax=1000;                     // full tank
float fuelFactor;                       // 
float fuelRemain;
float fuelFlow;                         // fuelFlow = throttle*fuelFactor
float oilPress;
float oilBase;
float oilFactor;                        // oilPress = oilBase+(throttle*oilFactor)
float h2oTemp;
float h2oBase;
float h2oFactor;                        // h2oTemp = h2oBase+(throttle*h2oFactor)
string warnList;                        // for warning lights, comma separated list
float tachRPM;
float tachFactor;
float tachIdle;                         // tachRPM = abs(throttle*tachFactor) (tachIdle is for idle only)                    
integer runTimer;                       // engine run time
float battBase;                         // battery base voltage (affected by current drain)
float battVolts;
float battFactor;                       // battVolts = battBase+abs(throttle*battFactor)
float chargeAmps;                       // ammeter charge or discharge
integer direction;                      // transmission's direction, 3 states.
float speed;                            // vehicle speed

// motor constant messages. Moved 10000 north of pbs to remove interference with the sails

integer bbkMsgStop  = 10010;             // stop engine
integer bbkMsgStart = 10020;            // start
integer bbkMsgThrottle = 10021;         // throttle message, string will contain value
integer bbkMsgFuel = 10022;             // fuel message string has value
integer bbkMsgOil = 10023;              // oil pressure
integer bbkMsgH2oTemp = 10024;
integer bbkMsgWarn = 10025;
integer bbkMsgTach = 10026;
integer bbkMsgVolts = 10027;
integer bbkMsgAmps = 10028;
integer bbkMsgDirection = 10029;
integer bbkMsgWake = 10030;
integer bbkMsgSmoke = 10031;
integer bbkMsgWindVane = 10032;
integer bbkMsgWindSpeed = 10033;

// engine variables
float thrust = 0.10;
float throttleMax = 10.0;
float throttle;
float thrustMult = 1.2;
integer dispThrot;
integer thrustPercent = 0;
vector linear_motor;
vector angular_motor;
float speedRatio;
float speedTurn;


// ------------------------- END GLOBAL BOAT SETTINGS ---

///////////////////////////////////////////////////////
// BWIND DEFAULT PRESET DECLARATION //////////////////
// modify this section as per 15 Knots Wind Preset //
////////////////////////////////////////////////////

//The boat will sail a 15 Knots East BWind by default... following parameters apply

float windDir=0; //BWind Wind direction 
string windRose="East ";   //BWind Preset declaration for 15 Knots Wind
string windType="15 Knots";     //BWind Preset declaration for 15 Knots Wind

//primary parameters
float initwindSpeed=7.75; // Don't touch this... 15 Knots Speed
float initmaxSpeed=5.5;   // Edit this according to your needs... this is the actual Speed with a 15 Knots Wind
float initheelTweak=0.85; // Edit this according to your needs... this is the actual Heel for a 15 Knots Wind

float windSpeed;
float maxSpeed;
float heelTweak;

// --------------------------- END DEFAULT PRESET DECLARATION ---

////////////////////////////////////////////////
// GLOBAL BOAT EQUATIONS AND FUNCTIONS   ///////
////////////////////////////////////////////////

// Modify at your own risk.

// Autotack routines. The first one will take the current heading and set your new tack heading
// based on the TWA. Also this handles gybe(or jibe) for downwind maneuvers. 

calcAutoTack()
{
    integer currTack;
    integer downwind=FALSE;                   // if we're downwind it isn't tacking it is gybing
    float dispTWA;
    dispTWA = llFabs(truewindAngle*RAD_TO_DEG);
    
    
    float currTWIangle = llFabs(truewindAngle);
    float currHeading = courseHoldValue*DEG_TO_RAD;
    float tackHeading;

    
    if (dispTWA > 90.0) downwind = TRUE;   // reverse all the course calculations
    
    if (downwind)
    {
        currTWIangle = (dispTWA /2.0) * DEG_TO_RAD;
        dispTWA = (currTWIangle * RAD_TO_DEG);
    }
    
    if (currBoomAngle < 0)          // tack to port, gybe to stbd
    {
        if (downwind) tackHeading = currHeading - currTWIangle ;
        else tackHeading = currHeading + (currTWIangle*2);
    }
    else
    {
        if (downwind) tackHeading = currHeading + currTWIangle;
        else tackHeading = currHeading - (currTWIangle*2);
    }
    
    tackHeading = tackHeading*RAD_TO_DEG;
    if (tackHeading > 360.00) tackHeading -= 360.0;
    
    courseHoldValue = llFabs(tackHeading);
    if (downwind)
    {
        llOwnerSay("New gybe heading is "+(string)courseHoldValue);
    }
    else
    {
        llOwnerSay("New tack heading is "+(string)courseHoldValue);
    }
    courseHold=TRUE;        // set course hold so the boat will turn on its own
    autoTrim=TRUE;          // set auto trim so boat will trim its sail on its own
    setSTI("AUTOTACK");
    
}

// Check the progress of the current autotack maneuver

checkTack()
{ 
    float compass=zRotAngle*RAD_TO_DEG;
    compass = (450.0 - compass)%360.0;
    float compDiff = compass - courseHoldValue;     // we should be in degrees
    compDiff = llFabs(compDiff);
    
    if (compDiff < 5)
    {
        autoTack = FALSE;
        setSTI("HOLD");
        llOwnerSay("Maneuver Completed.");
    }
} 
    

initMotor()                 // taken from pbsLite (my engine script) now gone GPL too, that's how it works
{
        fuelRemain = 1000;
        fuelFactor = .01;
        oilBase = 50.0;
        oilFactor = 1.0;                         // oil pressure rise (per throttle tick)
        h2oBase = 95.0;
        h2oFactor = .4;                         // water temp rise (degrees per throttle tick)
        tachFactor = 240.0;                     // each throttle tick increases by this amount
        tachIdle = 720.0;                       // tach at idle
        battBase = 12.5;                        // battery base voltage
        battFactor = 0.15;                       // RPM factor        
 
}

motorStart()
{
    llSay(0,"Engine Start");
    direction = 0;
    throttle = 0;
    runTimer = 0;
    llMessageLinked(LINK_ALL_CHILDREN , bbkMsgVolts, "14.7", NULL_KEY);       // battery charge
    llMessageLinked(LINK_ALL_CHILDREN , bbkMsgAmps, "30", NULL_KEY);       // current to charge battery
    llMessageLinked(LINK_ALL_CHILDREN , bbkMsgThrottle, (string)throttle, NULL_KEY);       // throttle
    llMessageLinked(LINK_ALL_CHILDREN , bbkMsgDirection, (string)direction, NULL_KEY);       // transmission
    llMessageLinked(LINK_ALL_CHILDREN , bbkMsgSmoke, "ON", NULL_KEY);       // exhaust
}

motorStop()
{
    llSay(0,"Engine Shutdown");
    throttle = 0;
    direction = 0;
    runTimer = 0;
    llMessageLinked(LINK_ALL_CHILDREN , bbkMsgStop, "stop", NULL_KEY);    // shutdown boat instruments etc
    llMessageLinked(LINK_ALL_CHILDREN , bbkMsgVolts, "8.0", NULL_KEY);       // battery drain
    llMessageLinked(LINK_ALL_CHILDREN , bbkMsgAmps, "0", NULL_KEY);       // current to start engine 
    llMessageLinked(LINK_ALL_CHILDREN , bbkMsgSmoke, "OFF", NULL_KEY);       // exhaust
    llMessageLinked(LINK_ALL_CHILDREN , bbkMsgWake, "OFF", NULL_KEY);       // wake
    
}

motorRun()
{
    runTimer +=1;
    if (throttle < 0) direction = -1;
    else if (throttle > 0) direction = 1;
    else direction = 0;
    thrust = throttle * (thrustMult * thrustMult);
    thrustPercent = (integer)((throttle / throttleMax) * 100.0);
    //llSetVehicleVectorParam(VEHICLE_ANGULAR_MOTOR_DIRECTION, angular_motor);
    speed = llVecMag(llGetVel());
    speedRatio = speed/maxSpeed;
    speedTurn=1.2*(speedRatio/1.5);
    
    linear_motor.x = thrust + (-angular_motor.x/2.5);
    linear_motor.y = 0.0;
    linear_motor.z = 0.0;
    llSetVehicleVectorParam(VEHICLE_LINEAR_MOTOR_DIRECTION, linear_motor);
    
    if (throttle == 0) tachRPM = tachIdle;
    else tachRPM = llFabs(throttle*tachFactor);
    fuelFlow = 0;
    oilPress = 100.0;
    h2oTemp = 120.0;
    chargeAmps = 5.0;
    battVolts = 14.2;

    motorInstruments();
    
}

motorInstruments()
{
    
    llMessageLinked(LINK_ALL_CHILDREN, bbkMsgFuel, (string)fuelRemain, NULL_KEY);
    llMessageLinked(LINK_ALL_CHILDREN, bbkMsgThrottle, (string)throttle, NULL_KEY);
    llMessageLinked(LINK_ALL_CHILDREN, bbkMsgTach, (string)tachRPM, NULL_KEY);
    llMessageLinked(LINK_ALL_CHILDREN, bbkMsgDirection, (string)direction, NULL_KEY);  // transmission
    llMessageLinked(LINK_ALL_CHILDREN, bbkMsgOil, (string)oilPress, NULL_KEY);
    llMessageLinked(LINK_ALL_CHILDREN, bbkMsgH2oTemp, (string)h2oTemp, NULL_KEY);
    llMessageLinked(LINK_ALL_CHILDREN, bbkMsgVolts, (string)battVolts, NULL_KEY);
    llMessageLinked(LINK_ALL_CHILDREN, bbkMsgAmps, (string)chargeAmps, NULL_KEY);
}

    
//  BWind Basic Boat Behaviour - calculate wind angle

//TRUE WIND - True Wind has been left as a separate routine in case you dont want your boat to support Apparent Wind
calcTrueWindAngle() { 
    currRot=llGetRot(); 
    currEuler=llRot2Euler(currRot); 
    zRotAngle=currEuler.z;//boat heading 
    leftVec=llRot2Left(currRot); 
    truewindAngle=windDir-zRotAngle; 
    while (truewindAngle>PI) truewindAngle-=TWO_PI;     //bw -PI and PI 
    while (truewindAngle<-PI) truewindAngle+=TWO_PI;    //bw -PI and PI
//  in order for the TWI gauge to work properly it needs to know negative
//  deflection, therefore we don't want to use absolute value (all of this could
//  be simplified with llFabs() but whatever...    

    if (TWI1 > 0)
    {
        setTWI(truewindAngle*RAD_TO_DEG);
    } 
}

//APPARENT WIND
calcAppWindAngle() { 
    currRot=llGetRot(); 
    currEuler=llRot2Euler(currRot); 
    zRotAngle=currEuler.z;  //boat heading 
    leftVec=llRot2Left(currRot); 
    windAngle=windDir-zRotAngle;
    while (windAngle>PI) windAngle-=TWO_PI;     //bw -PI and PI 
    while (windAngle<-PI) windAngle+=TWO_PI;    //bw -PI and PI 
    vector boatMovement=<currSpeed*llCos(currEuler.z),currSpeed*llSin(currEuler.z),0>;
    tmpwind=wind+boatMovement;
    float spd=llVecMag(llGetVel());
    appwindAngle=llAtan2(windSpeed*llSin(windAngle),(windSpeed*llCos(windAngle)+spd));
    while (appwindAngle>PI) spd=-spd;
    while (appwindAngle<-PI) spd=-spd;
    appwindSpeed=spd*llCos(llFabs(windAngle))-windSpeed*llCos(windAngle);
    if ((appwindSpeed) < 0) {
        appwindSpeed = appwindSpeed * -1;
    }
    if ((appwindSpeed) > 3) {
        appwindSpeed = appwindSpeed -(TWO_PI-1.0);
    }
        if (AWI1 > 0)
    {
        setAWI(appwindAngle*RAD_TO_DEG);
    }

}
      
//  BWind Basic Boat Behaviour - calculate heel angle based on wind and sail settings
//  also figure out lifting the boat slightly out of the water, so the sides don't sink

calcHeelAngle() {
    heelAngle=llAsin(leftVec.z);
    if (SAIL_UP) 
    {
        if (llFabs(windAngle+sailingAngle)>3*DEG_TO_RAD) 
        {
            heelTorque=SAIL_UP*llSin(windAngle)
                        *llCos(heelAngle)*PI_BY_TWO
                        *(windSpeed/maxWindSpeed)
                        *llCos(sailingAngle*DEG_TO_RAD)
                        *heelTweak;
        }
        else
        {
            heelTorque=0;
        }
    } else heelTorque=0;
    heelAdd=heelTorque-heelAngle;
    eulerRot=<heelAdd,0,0>;
    quatRot=llEuler2Rot(eulerRot);
    heelHeight = calcHeelHeight(heelTorque);
}

float calcHeelHeight(float Torque)
{
    float realTorque=Torque*RAD_TO_DEG;
    float angleC = llFabs(realTorque - 90.0)*DEG_TO_RAD; // angle 2
    float heelZ = llFabs(((beam / llSin(angleC)) * llSin(Torque)));
    return heelZ;
}
    

//  BWind Basic Boat Behaviour - calculate angle of sail (or jib) based on sheet setting and the wind

calcBoomDelta() {
    if (sheetAngle<=0) sheetAngle=5; //never let the actual sheetangle be less than 5°
    if (sheetAngle>=maxAngle) sheetAngle=maxAngle; //never let the actual sheetangle be more than maxAngle
    sailingAngle=sheetAngle;
    if (sailingAngle>llFabs(windAngle*RAD_TO_DEG)) sailingAngle=llRound(llFabs(windAngle*RAD_TO_DEG));
    if (windAngle<0) sailingAngle*=-1;
    delta=sailingAngle-currBoomAngle;
    currBoomAngle=sailingAngle;
    if (currBoomAngle < 0) Tack="Starb'd";
    if (currBoomAngle > 0) Tack="Port";
    llMessageLinked(SAIL,delta,"",NULL_KEY);//tell sail to rotate by delta
    llMessageLinked(JIB,delta,"",NULL_KEY);//tell jib to rotate by delta
    llMessageLinked(BOOM,delta,"",NULL_KEY);//tell boom to rotate by delta
    if (SAI1 > 0)
    {
        setSAI(sailingAngle);
    }
}

//  BWind Basic Boat Behaviour - calculate boat speed

calcSpeed() {
    groundSpeed=llGetVel();
    absWindAngle=llFabs(windAngle);
    if (llFabs(absWindAngle*RAD_TO_DEG-llFabs(sailingAngle))<10) trimFactor=0;
    else {
        optBoomAngle=0.5*absWindAngle*RAD_TO_DEG;
        trimFactor=(90.-llFabs(optBoomAngle-llFabs(sailingAngle)))/90.;
    }

    if (llFabs(appwindAngle)<ironsAngle*DEG_TO_RAD) currSpeed*=slowingFactor;
    else {
        if (SAIL_UP) {
            currSpeed=speedTweak*(llSin(llFabs(windAngle)/2)+llCos(llFabs(windAngle)/2.75) - 0.75)*windSpeed*trimFactor; // mod donated by Eta Carver
        }
        else currSpeed*=0.8;
    }
}

//  BWind Basic Boat Behaviour - calculate leeway (lateral drift) due to wind

calcLeeway() {
    leeway=SAIL_UP*-llSin(llFabs(appwindAngle))*llSin(heelAngle)*windSpeed*leewayTweak;     
    //BUG found by Balduin Aabye - ty :))
}


//  BWind Basic Boat Behaviour - calculate turning rate based on current speed
calcTurnRate() {
    spdFactor=llVecMag(groundSpeed)/(maxSpeed);
    rotSpeed=0.5+(spdFactor)/2.0;
}

// String Conversions - automatically detect link nums for each named part

getLinkNums() {
    integer i;
    integer linkcount=llGetNumberOfPrims();  
    for (i=1;i<=linkcount;++i) {
        string str=llGetLinkName(i);
        if (str=="jib") JIB=i;
        if (str=="sail") SAIL=i;
        if (str=="boom") BOOM=i;
        if (str=="pole") POLE=i;
        if (str=="spinnaker") SPINNAKER=i;
        if (str=="hud") HUD=i;
        if (str=="crewman") CREWMAN=i;
        if (str=="fenders") FENDERS=i;
        if (str=="fender2") FENDER2=i;
        if (str=="windvane") WINDVANE=i;
        if (str=="hatchupper") HATCHUPPER=i;
        if (str=="hatchlower") HATCHLOWER=i;
        if (str=="l33") L33 = i;
        if (str=="awi1") AWI1 = i;
        if (str=="twi1") TWI1 = i;
        if (str=="sai1") SAI1 = i;
        if (str=="genoa") GENOA = i;
        if (str=="sti1") STI1 = i;
        if (str=="ins1") INS1 = i;

    }

}

// handle fenders and hatches. 
fenders(float visible)
{
    float openshut;         // hatch does the opposite
    
    if (visible > 0)
    {
        openshut = 0.0;
    }
    else
    {
            openshut = 1.0;
    }

    // 1-40 make sure they are there
   if (FENDERS != 0) llSetLinkAlpha(FENDERS,visible,ALL_SIDES);
   if (FENDER2 != 0) llSetLinkAlpha(FENDER2,visible,ALL_SIDES);
   if (L33 != 0) llSetLinkAlpha(L33,visible,ALL_SIDES);
   if (HATCHUPPER != 0) llSetLinkAlpha(HATCHUPPER,openshut,ALL_SIDES);
   // llSetLinkPrimitiveParamsFast(HATCHUPPER,[PRIM_PHANTOM,(integer)visible]);
   if (HATCHLOWER != 0) llSetLinkAlpha(HATCHLOWER,openshut,ALL_SIDES);
   // llSetLinkPrimitiveParamsFast(HATCHLOWER,[PRIM_PHANTOM,(integer)visible]);
}

// gauge setting functions, these all send a message to some gauge
// your gauge must translate this into some readable number somehow. 
// Look in the kit for some example gauges. 

setSAI(integer boomAngle)                                 // sail angle indicator
{
    llMessageLinked(SAI1,24,(string)boomAngle,"");
}

setAWI(float appAngle)                                  // Apparent Wind Indicator
{
    llMessageLinked(AWI1,22,(string)appAngle,"");
}

setTWI(float twiAngle)                                  // True Wind Indicator
{
    llMessageLinked(TWI1,23,(string)twiAngle,"");
}

setSTI(string indication)
{
    llMessageLinked(STI1,25,indication,"");             // Sail Trim Indicator
}

setINS1(float wSpeed, float wDir)
{
    llMessageLinked(INS1,bbkMsgWindVane,(string)wDir,"");
    llMessageLinked(INS1,bbkMsgWindSpeed,(string)wSpeed,"");
}


setWindvane(float wDir)                                 // Windvane if present
{
    // given a direction, send it to the windvane, it'll point in that general direction.
    llMessageLinked(WINDVANE,0,(string)wDir,"");
}

// use the WeatherOS/OE buoy

setWindFromBuoy()
{
    windh = llListen(-54001,"","","");
}
            

// RAISE ROUTINE - raise sail: start timer

raiseSail() {
    SAIL_UP=TRUE;
    llOwnerSay("Ready to sail!");
    
    llMessageLinked(SAIL,1002,"",NULL_KEY); // raise sail
    llMessageLinked(JIB,1002,"",NULL_KEY); // raise jib
    
    
    llMessageLinked(SPINNAKER,1001,"",NULL_KEY); // don't raise spinnaker
    llMessageLinked(GENOA,1001,"",NULL_KEY);    // don't raise genoa either
    genoaUp = FALSE;
    
    
    llMessageLinked(BOOM,1002,"",NULL_KEY); // move boom
    llMessageLinked(CREWMAN,1000,"",NULL_KEY); // hide crewman poseball
    fenders(0.0);                               // hide fenders, shut hatches
    
    llSetTimerEvent(timerFreq);
    llLoopSound("sailing", 1.0);       

} 

// LOWER ROUTINE - lower sail but leave physics on

lowerSail() {
    llOwnerSay("Lowering sails.");
    llMessageLinked(SAIL,1000,"",NULL_KEY);//lower sail w/ reset
    llMessageLinked(JIB,1000,"",NULL_KEY);//lower jib w/ reset
    llMessageLinked(BOOM,1000,"",NULL_KEY);//stop boom w/ reset
    llMessageLinked(POLE,1000,"",NULL_KEY);//stop pole w/ reset
    llMessageLinked(SPINNAKER,1000,"",NULL_KEY); // drop and reset spinnaker
    llMessageLinked(GENOA,1000,"",NULL_KEY);    // drop and reset genoa
    fenders(1.0);                               // put the fenders out, open the hatches
    sailingAngle = 0;
    llStopSound();
    llMessageLinked(LINK_ALL_CHILDREN , 0, "stop", NULL_KEY); // stop all children-linked activities
    currBoomAngle=0;
    sheetAngle=5;
    SAIL_UP=FALSE; 

}

// genoa routines

HoistGen()
{
    if (SPIN_UP)
    {
        DropSpin();
    }
    if (GENOA !=0)
    {
        llOwnerSay("Hoisting Genoa");
        llMessageLinked(GENOA, 1002, "", NULL_KEY);
        genoaUp = TRUE;
    }
    else
    {
        llOwnerSay("No genoa sail to hoist!");
    }
}

DropGen()
{
    if (GENOA !=0)
    {
        llOwnerSay("Dropping Genoa");
        llMessageLinked(GENOA, 1000, "", NULL_KEY);
        genoaUp = FALSE;
    }
}

        

//SPINNAKER ROUTINE
HoistSpin(){
    if ((sheetAngle > 50)) {
        llOwnerSay("Hoisting Spinnaker");
        llMessageLinked(SPINNAKER,1002,"",NULL_KEY); //hoist spinnaker
        if (-sailingAngle > 0) llMessageLinked(POLE,1002,"",NULL_KEY);
        if (-sailingAngle < 0) llMessageLinked(POLE,1004,"",NULL_KEY);
        llMessageLinked(JIB,1001,"",NULL_KEY); //lower jib, keep position
        SPIN_UP=TRUE;
//        CurJib=0;
    }
    if ((sheetAngle < 50)) {
        llOwnerSay("Sheet too close to Hoist Spin");
    }
}
DropSpin(){
    llOwnerSay("Dropping Spinnaker");
    llMessageLinked(SPINNAKER,1000,"",NULL_KEY); //drop spinnaker/W reset
    llMessageLinked(POLE,1000,"",NULL_KEY); // hide & reset pole
    llMessageLinked(JIB,1002,"",NULL_KEY); //raise jib
    CurSpin=0;
    SPIN_UP=FALSE;
}
TrimSpinPlus() {
    if (CurSpin < 37) {
        llMessageLinked(SPINNAKER,(delta+3),"",NULL_KEY); //trim spinnaker +
        llMessageLinked(POLE,(delta+3),"",NULL_KEY); // hide pole
    CurSpin=CurSpin+3;
    } 
    
}
TrimSpinMinus() {
    if (CurSpin > -37) {
        llMessageLinked(SPINNAKER,(delta-3),"",NULL_KEY); //trim spinnaker -
        llMessageLinked(POLE,(delta-3),"",NULL_KEY); // hide pole 
        CurSpin=CurSpin-3;
    }
}

// VEHICLE PHYSICS PARAMETERS - set initial vehicle parameters

// WARNING !!!! - Following physics parameters should NOT be edited unless you REALLY KNOW what you are doing....

setVehicleParams() {
    //vehicle flags
    llSetVehicleType         (VEHICLE_TYPE_BOAT);
    llSetVehicleRotationParam(VEHICLE_REFERENCE_FRAME,ZERO_ROTATION); // ZERO_ROTATION = <0.0,0.0,0.0,1.0> you may wish to edit this for fun
    llSetVehicleFlags        (VEHICLE_FLAG_NO_DEFLECTION_UP|VEHICLE_FLAG_HOVER_GLOBAL_HEIGHT|VEHICLE_FLAG_LIMIT_MOTOR_UP ); 
    //linear motion
    llSetVehicleVectorParam  (VEHICLE_LINEAR_FRICTION_TIMESCALE,<50.0,2.0,0.5>);;
    llSetVehicleVectorParam  (VEHICLE_LINEAR_MOTOR_DIRECTION,ZERO_VECTOR);
    llSetVehicleFloatParam   (VEHICLE_LINEAR_MOTOR_TIMESCALE,10.0);
    llSetVehicleFloatParam   (VEHICLE_LINEAR_MOTOR_DECAY_TIMESCALE,60);
    llSetVehicleFloatParam   (VEHICLE_LINEAR_DEFLECTION_EFFICIENCY,0.85);
    llSetVehicleFloatParam   (VEHICLE_LINEAR_DEFLECTION_TIMESCALE,1.0); 
    //angular motion
    llSetVehicleVectorParam  (VEHICLE_ANGULAR_FRICTION_TIMESCALE,<5,0.1,0.1>);
    llSetVehicleVectorParam  (VEHICLE_ANGULAR_MOTOR_DIRECTION,ZERO_VECTOR);
    llSetVehicleFloatParam   (VEHICLE_ANGULAR_MOTOR_TIMESCALE,0.1);
    llSetVehicleFloatParam   (VEHICLE_ANGULAR_MOTOR_DECAY_TIMESCALE,3);
    llSetVehicleFloatParam   (VEHICLE_ANGULAR_DEFLECTION_EFFICIENCY,1.0);
    llSetVehicleFloatParam   (VEHICLE_ANGULAR_DEFLECTION_TIMESCALE,1.0);//default 1.0 -- reduce to have more lateral drift (like 0.3 - 0.5)
    //vertical attractor
    llSetVehicleFloatParam   (VEHICLE_VERTICAL_ATTRACTION_TIMESCALE,3.0);
    llSetVehicleFloatParam   (VEHICLE_VERTICAL_ATTRACTION_EFFICIENCY,0.8);
    //banking
    llSetVehicleFloatParam   (VEHICLE_BANKING_EFFICIENCY,0.0);
    llSetVehicleFloatParam   (VEHICLE_BANKING_MIX,1.0);
    llSetVehicleFloatParam   (VEHICLE_BANKING_TIMESCALE,1.2);
    //vertical control
    llSetVehicleFloatParam   (VEHICLE_HOVER_HEIGHT,seaLevel+keelTrim); // floats based on keelTrim
    llSetVehicleFloatParam   (VEHICLE_HOVER_EFFICIENCY,2.0);
    llSetVehicleFloatParam   (VEHICLE_HOVER_TIMESCALE,1.0);
    llSetVehicleFloatParam   (VEHICLE_BUOYANCY,1.0);
}

//MASTMAN AND CREW CAMERA SETUP - set camera position for third person view

setCamera() {
    llSetCameraEyeOffset(<-21.5,0.0,4.7>); //Here you may set your Sailing Camera EYE Offset
    llSetCameraAtOffset(<4.0,0.0,1.0>);    //Here you may set your Sailing Camera actual position
}

//REZZING INITIAL POSITION - figure out where to put boat when it is rezzed
// 
setInitialPosition() {
    vector pos=llGetPos();
    float groundHeight=llGround(ZERO_VECTOR);
    float waterHeight = llWater(ZERO_VECTOR);
    seaLevel=llWater(ZERO_VECTOR);
    upright();
   // adjusts the float height. That keelTrim is where you need to set to adjust.
    if (groundHeight <= waterHeight) {
        pos.z = waterHeight + keelTrim; // the keeltrim will set your float offset
        while (llVecDist(llGetPos(),pos)>.001) llSetPos(pos);
    }
}

//SIT TARGET
//this function is a stub. Use either the bwind pose or avsitter or something.

setSitTarget() {
    llSetSitText("Sail !");
    llSetText("",ZERO_VECTOR,1.0);
}

//BOAT UPRIGHT POSITION - force boat upright

upright() {
    currRot=llGetRot();
    currEuler=llRot2Euler(currRot);
    leftVec=llRot2Left(currRot);
    heelAngle=llAsin(leftVec.z);
    eulerRot=<-heelAngle,0,0>;
    quatRot=llEuler2Rot(eulerRot);
    llRotLookAt(quatRot*currRot,0.2,0.2);
}

//MOORING FUNCTION - what happens when your boat moors...

moor() {
    llMessageLinked(LINK_THIS,0,"moor",NULL_KEY);
    {
        llSetTimerEvent(timerFreq);
        sailing=TRUE;
    }
    llOwnerSay("Mooring.");
    upright();
    llReleaseControls();
    llSetStatus(STATUS_PHYSICS,TRUE);
    llSetTimerEvent(0);
    currSpeed=0;
    fenders(1.0); 
}

//GENERAL BOAT STARTUP - reset stuff - this resets the script to defaults.

startup() {
    owner=llGetOwner();
    llSetStatus(STATUS_ROTATE_X | STATUS_ROTATE_Z | STATUS_ROTATE_Y,TRUE);
    llSetStatus(STATUS_PHYSICS,FALSE);
    llSetStatus(STATUS_PHANTOM,FALSE);
    llSetStatus(STATUS_BLOCK_GRAB,TRUE);
    llSetTimerEvent(0);
    setInitialPosition();
    setVehicleParams();
    setSitTarget();
    getLinkNums();                               
    llMessageLinked(SAIL,1000,"",NULL_KEY);     //reset MAINSAIL
    llMessageLinked(JIB,1000,"",NULL_KEY);      //reset JIB
    llMessageLinked(BOOM,1000,"",NULL_KEY);      //reset BOOM
    llMessageLinked(CREWMAN,1001,"",NULL_KEY); // show crewman poseball
    llMessageLinked(POLE,1000,"",NULL_KEY); // hide & reset pole
    llMessageLinked(SPINNAKER,1001,"",NULL_KEY); //reset SPINNAKER
    llMessageLinked(GENOA,1000,"",NULL_KEY);    // reset GENOA
    setCamera(); //apply Camera default setting
    currSpeed=0;
    windSpeed=initwindSpeed; // 
    maxSpeed=initmaxSpeed;   // 
    heelTweak=initheelTweak; // 

    llListen(0,"",owner,""); //listen to boat owner only... 
    llMessageLinked(LINK_ALL_CHILDREN , 0, "stop", NULL_KEY);
    llOwnerSay(boatName + " " + versionNumber + " Ready.");
    setSTI("DISABLED");
}

// 1-38 heading hold (not really course hold, but keeps you pointed in some direction
//
updateCourse()
{
    float compass=zRotAngle*RAD_TO_DEG;
    compass = (450.0 - compass)%360.0;
    float compDiff = compass - courseHoldValue;     // we should be in degrees
    float turnMult;
    
    if (autoTack)
    {
        turnMult=2.0;       // turn far more aggressively while in autotack
    }
    else
    {
        turnMult=35.0;
    }
    
    if (llFabs(compDiff) > 359.00) // for close to northerly headings, be careful so you don't spin
    {
        compass = compass + 360.00;
    }
    
    if (compass > (courseHoldValue+1))
    {
        llSetVehicleVectorParam(VEHICLE_ANGULAR_MOTOR_DIRECTION,<0.0,0.0,rotSpeed/turnMult>);
    }
    else
    {
        if (compass < (courseHoldValue-1))
        {
           llSetVehicleVectorParam(VEHICLE_ANGULAR_MOTOR_DIRECTION,<0.0,0.0,-rotSpeed/turnMult>);
        }
    }
}


// ------------------------------------- END GLOBAL BOAT EQUATIONS AND FUNCTIONS---

///////////////////////////////////////////////////
// HUD SETTINGS //////////////////////////////////
// you don't want to modify these settings //////
////////////////////////////////////////////////

// UPDATE HUD - Main HUD routine and colour management
updateHUD() {
    string dataString;
    float compass=PI_BY_TWO-zRotAngle;
    float effcoeff;
    
    string rgn = llGetRegionName();
    string blank = " ";
    float efficiency;
    string ratio;
    float derivatesheetAngle=sheetAngle;
    
    //CALCULATE SPINNAKER EFFECTS ON EFFICIENCY

    //Spinnaker tweak weighs 1/3 of global sheet
    if (Tack=="Starb'd") {
        derivatesheetAngle+=(CurSpin/3); //Starboards Spinnaker Tweak
    }
    if (Tack=="Port") {
        derivatesheetAngle-=(CurSpin/3); //Port Spinnaker Tweak
    }

    //ACTUAL EFFICIENCY CALCULATION including spinnaker tweaks
    efficiency = llFabs(appwindAngle)*RAD_TO_DEG/derivatesheetAngle;
    if ((efficiency) < 0) {
        efficiency = efficiency * -1;
    }
    
    compass=PI_BY_TWO-zRotAngle;
    while (compass<0) compass+=TWO_PI;
    dataString = " "; //clean hud message on startup
    currentString = " "; //clean hud storage on startup
    
    //HUD COMPASS CONVERSION
    
    float hudcompass =((integer)(compass*RAD_TO_DEG));
    string huddirection;
    if ((hudcompass) <= 360) {    
        huddirection="North";
    }
    if ((hudcompass) <= 315) {    
        huddirection="Northwest";
    }
    if ((hudcompass) <= 275) {    
        huddirection="West";
    }
    if ((hudcompass) <= 230) {    
        huddirection="Southwest";
    }
    if ((hudcompass) <= 185) {    
        huddirection="South";
    }
    if ((hudcompass) <= 140) {    
        huddirection="SouthEast";
    }
    if ((hudcompass) <= 95) {    
        huddirection="East";
    }
    if ((hudcompass) <= 50) {    
        huddirection="Northeast";
    }
    if ((hudcompass) <= 5) {    
        huddirection="North";
    }
         
    // STANDARD HUD STRING CONSTRUCTION ROUTINE
    
    if (ADV_HUD==FALSE) {
    
    // efficiency positive convert and ratio calculation
    if ((efficiency) < 0) {
        efficiency = efficiency * -1;
    }
    ratio = llGetSubString ((string)efficiency, 0, 3);
       
    hudcolour=<1,1,1>;                                                             //default hudcolour
    dataString+="-> "+huddirection+" ";                                                        //add a direction string
    dataString+="( " +(string)((integer)(compass*RAD_TO_DEG))+"° )\n ";                       //add compass degrees           
    dataString+="| "+visualAid+" | Speed "+llGetSubString((string)(llVecMag(groundSpeed*convert)),0,3)+units+"\n";   //add boat speed
    dataString+=(windType)+" "+(windRose)+"BWind\n";                                        //add BWind wind preset
    
   //trailing blanks
    dataString+=blank+"\n";
    dataString+=blank+"\n";
    
    }
    
    // ADVANCED HUD STRING CONSTRUCTION ROUTINE
    
    if (ADV_HUD==TRUE) {
        
    // efficiency positive convert
    if ((efficiency) < 0) {
        efficiency = efficiency * -1;
    }
     
    hudcolour=<1,1,1>;                                                             //default hudcolour
    dataString+="-> "+huddirection+" ";                                                        //add a direction string
    dataString+="( " +(string)((integer)(compass*RAD_TO_DEG))+"° )\n ";                       //add compass degrees             
    dataString+="| "+visualAid+" | Speed "+llGetSubString((string)(llVecMag(groundSpeed*convert)),0,3)+units+" - ";   //add boat speed
    dataString+=(windType)+" "+(windRose)+"BWind\n";                                        //add BWind wind preset
    dataString+="True Wind Angle " +(string)((integer)(llFabs(truewindAngle)*RAD_TO_DEG))+"° - ";  //add True Wind
    dataString+="App. Wind Angle " +(string)((integer)(llFabs(appwindAngle)*RAD_TO_DEG))+"°\n";   //add Apparent Wind 
           
    ratio = llGetSubString ((string)efficiency, 0, 3);                                   //display efficiency conversion
    dataString+="Wind/Sheet ratio " +ratio+ " - Sheet Angle "+((string)(sheetAngle))+"°\n"; //add Efficiency and Sheet Angle

   //trailing blanks
    dataString+=blank+"\n";
    dataString+=blank+"\n";

    
    }
    
    //---------- END HUD CONSTRUCTION ROUTINE
    
    // HUD Colour Manager - This activates the HUD's virtual telltale function and affects ACTUAL BOAT'S SPEED WHILE TRIMMING
    // THIS ROUTINE IS VITAL FOR YOUR BOAT PERFORMANCE... THIS SETS THE BOAT'S SPEED WHEN YOU TRIM SAILS
    
    // too tight - cyan - this is good as it is...
    if ((efficiency < 25.0) && (efficiency > 2.5)) 
    {    
        hudcolour=<0,1,1>;
        visualAid="<>"; //help symbol for colour visual impaired - immediate feel of TOO TIGHT
        speedTweak=0.3; //MODIFY THIS VALUE ACCORDING TO YOUR NEEDS... TWEAKS BOAT'S SPEED 
        llMessageLinked(LINK_ALL_CHILDREN,1010,"flap",NULL_KEY); // flap the sails. All sails better listen to this message
        
        if (autoTrim) sheetAngle += 2;                           // if Auto Trim set, adjust.
        if (!autoTack) setSTI("LOOSEN");                        
            
    }
    
    // optimal - green - INCREASE THIS (never over 3) for easier apparent wind sailing
    // LOWER THIS (never below 2) for a more demanding trim activity
    if ((efficiency <= 2.5) && (efficiency > 1.7)){   
        hudcolour=<0,1,0>;
        visualAid="="; //help symbol for colour visual impaired - immediate feel of OPTIMAL
        speedTweak=1.0; //MODIFY THIS VALUE ACCORDING TO YOUR NEEDS... TWEAKS BOAT'S SPEED
        llMessageLinked(LINK_ALL_CHILDREN,1010,"green",NULL_KEY);
        if (!autoTack) setSTI("TRIMMED");

    }
    
    // off optimal - yellow - here the sheet is too loose, but your boat will still sail quite well
    if ((efficiency <= 1.7) && (efficiency > 1.2)) {    
        hudcolour=<1,1,0>;
        visualAid="><"; //help symbol for colour visual impaired - immediate feel of TOO LOOSE
        speedTweak=0.7; //MODIFY THIS VALUE ACCORDING TO YOUR NEEDS... TWEAKS BOAT'S SPEED 
        llMessageLinked(LINK_ALL_CHILDREN,1010,"flap",NULL_KEY);    // flap sails
        if (!autoTack) setSTI("TIGHTEN");
        if (autoTrim) sheetAngle -= 2;                           // if Auto Trim set, adjust.
    }
    
    // too loose - red - oh yes this is very bad... the boat will almost stop sailing :)
    if ((efficiency) <= 1.2) {    
        hudcolour=<1,0,0>;
        visualAid=">><<"; //help symbol for colour visual impaired - immediate feel of WAY TOO LOOSE
        speedTweak=0.3; //MODIFY THIS VALUE ACCORDING TO YOUR NEEDS... TWEAKS BOAT'S SPEED 
        llMessageLinked(LINK_ALL_CHILDREN,1010,"flap",NULL_KEY);    // flap sails
        if (!autoTack) setSTI("TIGHTEN");
        if (autoTrim) sheetAngle -= 5;                           // if AutoTrim set, adjust even more, it is way out.
    }
    
    //display hud string - HERE WE START HUD
    if (HUD_ON)
    {
        llSetText(dataString,(vector)hudcolour,1.0);
        currentString=dataString; //save current HUD message
    }
    else
    {
        llSetText("",<0.0,0.0,0.0>,0.0);
    }


    // SPINNAKER Boost/De-Boost  (moved out of the efficiency tweaks above)
    // important not to allow downwind speed to exceed wind speed. 
        if (SPIN_UP) 
        {
            if (sheetAngle > 65) speedTweak+=0.15; 
            if (sheetAngle < 65) speedTweak+=0.3;               
            if (sheetAngle < 50) speedTweak-=1.0; 
            if (speedTweak <= 0) speedTweak = 0.00001;   // this shouldn't really happen but let's avoid divide by 0         
        } 
    // GENOA Boost/De-Boost code. A genoa sail is the opposite of a spinnaker in behaviour. 
    // It is for nearer the irons operation.
    
    if (genoaUp)
    {
        if (sheetAngle > 5) speedTweak +=0.4;
        if (sheetAngle > 15) speedTweak -=0.2;
        if (sheetAngle > 30) speedTweak -=0.2;  // i.e. negate the advantage of the genoa at this point
        if (speedTweak <= 0) speedTweak = 0.00001; // this actually should not happen
    }

    //Tacking animation management 
    // just turn this on or off by setting Tacking=TRUE or not. Normally you won't need it if you use AVSitter for sits.
    if (Tacking)
    { 
        if (-sailingAngle > 0) {
            if ((llGetPermissions() & PERMISSION_TRIGGER_ANIMATION) && llGetAgentSize(llGetPermissionsKey()) != ZERO_VECTOR) {
                llStopAnimation("bbk_helmsman");
                llStartAnimation ("bbk_helmstack");
            }
        }
        if (-sailingAngle < 0) {
            if ((llGetPermissions() & PERMISSION_TRIGGER_ANIMATION) && llGetAgentSize(llGetPermissionsKey()) != ZERO_VECTOR) {
                llStopAnimation("bbk_helmstack");
                llStartAnimation ("bbk_helmsman");
            }
        }
    }   
}


// ---------------------------- END HUD SETTINGS ---

//////////////////////////////////////////////////////////////////////////////////////////////
// SCRIPT TRAPPING ROUTINE - state default //////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////

default {
    
    //get boat parts status
    state_entry() {
        getLinkNums();
        llSetText("",ZERO_VECTOR,1.0);
        startup();
        llSetStatus(STATUS_BLOCK_GRAB,TRUE);
    }
    
    //reset boat
    on_rez(integer param) {
        llResetScript();
    }

    //OWNER CHECK AND STATUS
    changed(integer change) {
        avatar=llAvatarOnSitTarget();
        if (change & CHANGED_LINK) {
            if (avatar==NULL_KEY) {
                if (!(llGetAgentInfo(owner) & AGENT_ON_OBJECT)) {
                    if (SAIL_UP) 
                    {
                        lowerSail();
                        llMessageLinked(SPINNAKER,1000,"",NULL_KEY); //drop spinnaker/W reset
                    }
                    if (permSet) llReleaseControls();
                    permSet=FALSE;
                    llMessageLinked(LINK_SET, 70400, "", NULL_KEY);
                    llResetScript();
                }
           }
           else {
                if (ownerVersion && avatar!=owner) llWhisper(0,"Only the owner can operate this boat.");
                else if ((llGetAgentInfo(owner) & AGENT_ON_OBJECT)) {
                    llMessageLinked(SPINNAKER,1000,"",NULL_KEY); //drop spinnaker/W reset
                    llWhisper(0,"Say raise to start sailing, help for sailing commands...");
                    llWhisper(0,"BWind System defaults to East Wind, 15 Knots...");
                    
                    if (llAvatarOnSitTarget()==owner) llRequestPermissions(owner,PERMISSION_TAKE_CONTROLS | PERMISSION_TRIGGER_ANIMATION);          
                }
            }
        }
    }    
    
    
    //BOAT CONTROLS SETUP
    run_time_permissions(integer perms) {
        if (perms & (PERMISSION_TAKE_CONTROLS)) {
            llTakeControls(CONTROL_RIGHT | CONTROL_LEFT | CONTROL_ROT_RIGHT |
            CONTROL_ROT_LEFT | CONTROL_FWD | CONTROL_BACK | CONTROL_DOWN | CONTROL_UP,TRUE,FALSE);
            permSet=TRUE;
            if (permSet) llStartAnimation("bbk_helmsman");
            llMessageLinked(LINK_SET, 70400, "", avatar);
            llMessageLinked(SPINNAKER,1000,"",NULL_KEY); //drop spinnaker/W reset 
        }
    }
    
    // ------------------------ END STATE DEFAULT DECLARATIONS ---
    
    ////////////////////////////////////////////////////////////////////
    // MAIN BOAT LISTENER /////////////////////////////////////////////
    //////////////////////////////////////////////////////////////////
    
    listen(integer channel, string name, key id, string msg) 
    {
        
        if (channel==0) 
        {
            if (owner==id & llAvatarOnSitTarget()==owner) 
            {
                if (llGetAgentInfo(owner) & AGENT_ON_OBJECT) 
                {
                    if (llGetSubString(msg,0,4)=="sheet")                   // add or subtract nn from sheet angle
                    {
                        incr=(integer)llDeleteSubString(msg,0,4);
                        sheetAngle+=incr;
                        if (sheetAngle>maxAngle) sheetAngle=maxAngle;
                    }
                    
                    //Raise sails, take boat into physical mode
                    else if (msg=="raise") 
                    {
                        if (motorOn)
                        {
                            motorStop();
                            fenders(1);
                            motorOn=FALSE;
                        }
                            
                        llMessageLinked(LINK_ALL_CHILDREN , 0, "start", NULL_KEY);
                        sailing=TRUE;
                        if (!permSet) llRequestPermissions(owner,PERMISSION_TAKE_CONTROLS | PERMISSION_TRIGGER_ANIMATION);
                        permSet=TRUE;
                        llSetStatus(STATUS_PHYSICS,TRUE);
                        raiseSail();
                        llSetTimerEvent(timerFreq);
                    }
                    else if (msg=="genoa" && genoaUp) DropGen();
                    else if (msg=="genoa" && !genoaUp) HoistGen();
                    
                    // motor mode. here we turn on the motor and the sails off.
                    
                    else if (msg=="motor")
                    {
                        if (sailing)                        // lower sails if up
                        {
                            if (SAIL_UP)
                            {
                               
                                sailing = FALSE;
                                SAIL_UP = FALSE;
                                lowerSail();                // drop the sails
                                llMessageLinked(SPINNAKER,1000,"",NULL_KEY); //drop spinnaker/W reset
                                llMessageLinked(POLE,1000,"",NULL_KEY); // hide & reset pole
                            }
                        llMessageLinked(LINK_ALL_CHILDREN , 0, "start", NULL_KEY);
                        if (!permSet) llRequestPermissions(owner,PERMISSION_TAKE_CONTROLS | PERMISSION_TRIGGER_ANIMATION);
                        permSet=TRUE;
                        llSetStatus(STATUS_PHYSICS,TRUE);
                        raiseSail();
                        lowerSail();
                        llMessageLinked(SPINNAKER,1000,"",NULL_KEY); //drop spinnaker/W reset
                        llMessageLinked(POLE,1000,"",NULL_KEY); // hide & reset pole
                        llSetTimerEvent(timerFreq);
                        fenders(0);                                
                        }
                        else                                // if we weren't sailing we need to make the boat physical
                        {
                            //sailing = FALSE;
                            SAIL_UP = FALSE;
                            if (!permSet) llRequestPermissions(owner,PERMISSION_TAKE_CONTROLS | PERMISSION_TRIGGER_ANIMATION);
                            permSet=TRUE;
                            llSetStatus(STATUS_PHYSICS,TRUE);
                            llSetTimerEvent(timerFreq);
                            raiseSail();
                            llSleep (0.2);
                            lowerSail();
                            llMessageLinked(SPINNAKER,1000,"",NULL_KEY); //drop spinnaker/W reset
                            llMessageLinked(POLE,1000,"",NULL_KEY); // hide & reset pole
                            fenders(0);
                        }
                        llMessageLinked(LINK_ALL_CHILDREN , 0, "start", NULL_KEY);
                        motorOn = TRUE;
                        sailing = FALSE;
                        initMotor();
                        motorStart();
                        fenders(0);
                    }
                               
                    // SPINNAKER HOIST/DROP
                    else if (msg=="spin" && SPIN_UP) DropSpin();
                    else if (msg=="spin" && !SPIN_UP) HoistSpin();

                    //SPINNAKER TRIM
                    else if (msg=="spin+") TrimSpinPlus();
                    else if (msg=="spin-") TrimSpinMinus();

                    //GYBE POLE
                    else if (msg=="gybe") 
                    {
                        if (SPIN_UP==TRUE) 
                        {
                            llMessageLinked(SPINNAKER,1004,"",NULL_KEY); //hoist spinnaker
                            if (-sailingAngle > 0) llMessageLinked(POLE,1003,"",NULL_KEY);
                            if (-sailingAngle < 0) llMessageLinked(POLE,1004,"",NULL_KEY);
                            CurSpin=0;
                        }
                    }
                    
                    //MESSAGE lower - okay now we want physics ON but no sailing.
                    else if (msg=="lower")
                    {
                        sailing=FALSE;
                        lowerSail();
                    }
                    else if (msg=="moor") 
                    {
                        if (motorOn)
                        {
                            motorOn = FALSE;
                            motorStop();
                        }
                        
                        llMessageLinked(LINK_ALL_CHILDREN , 0, "stop", NULL_KEY);
                        moor();
                        llSetTimerEvent(0);
                        if (SAIL_UP) lowerSail();
                        llResetScript();
                    }
                    // heading hold 
                    else if (msg=="hold")
                    {
                        courseHold = TRUE;
                        courseHoldValue = zRotAngle*RAD_TO_DEG;
                        courseHoldValue = (450.0 - courseHoldValue)%360.0;
                        llOwnerSay("Course Hold enabled:" + (string)courseHoldValue);
                    }
                    // auto trim enable
                    else if (msg=="trim")
                    {
                        autoTrim = TRUE;
                        llOwnerSay("Auto Trim enabled");
                    }
                    // auto tack enable
                    if (msg=="tack")
                    {
                        autoTack = TRUE;
                        llOwnerSay("Auto Tack Start");
                        courseHold = TRUE;
                        courseHoldValue = zRotAngle*RAD_TO_DEG;
                        courseHoldValue = (450.0 - courseHoldValue)%360.0;
                        
                        calcAutoTack();
                    }
                    
                    //BOAT ID Setter
                    else if (llGetSubString(msg,0,1)=="id") 
                    {
                        if (llGetSubString(msg,3,-1)!="off") 
                        {
                            idStr=llGetSubString(msg,3,-1);
                            string tmp=boatName+" #"+idStr;
                            llSetObjectName(tmp);
                            llOwnerSay("New Boat ID :"+tmp);
                        }
                    }
                    
                    //START bbk_helmsman ANIMATION
                    else if (llGetSubString(msg,0,3)=="anim") 
                    {
                        if (llGetSubString(msg,5,-1)=="off") 
                        {
                        }
                        else if (llGetSubString(msg,5,-1)=="on") 
                        {
                            if (permSet) llStartAnimation("bbk_helmsman"); // change thise pose/animation name if needed...
                        }
                    } 

                }
            }
            
            //-------------------------------- END MAIN BOAT LISTENER ---
            
            ///////////////////////////////////////////////////////////////
            // BWind Engine - WIND DIRECTION PRESETS /////////////////////
            //////////////////////////////////////////////////////////////
            
            //wind direction in degrees - North = 0°
            
            // setter on or off, locks out the rest of the settings
            
            if (msg=="setter") 
            {
                llOwnerSay("BWind using weather buoy");
                useSetter=TRUE;
                lock = TRUE;
                setWindFromBuoy();      // will set all the preset variables based on set wind vector
                
            }
            if (msg=="setoff") 
            {
                llOwnerSay("BWind setter disengaged");
                llListenRemove(windh);  // stop listening, save lag
                useSetter=FALSE;        // stop setting variables
                lock = FALSE;           // allow the manual BWind setter to work again
  
            } 
            
            // command set here
                      
            if (lock==FALSE) {
            
            if (msg=="n") {
                windRose ="North ";
                windDir=(90*DEG_TO_RAD);
                llOwnerSay("BWind now blowing from North");
            }
            if (msg=="nw") {
                windRose ="Northwest ";
                windDir=(135*DEG_TO_RAD);
                llOwnerSay("BWind now blowing from Northwest");
            }
            if (msg=="ne") {
                windRose ="Northeast ";
                windDir=(45*DEG_TO_RAD);
                llOwnerSay("BWind now blowing from Northeast");
            }
            if (msg=="e") {
                windRose ="East ";
                windDir=(0*DEG_TO_RAD);
                llOwnerSay("BWind now blowing from East");
            }
            if (msg=="s") {
                windRose ="South ";
                windDir=(270*DEG_TO_RAD);
                llOwnerSay("BWind now blowing from South");
            }
            if (msg=="sw") {
                windRose ="Southwest ";
                windDir=(225*DEG_TO_RAD);
                llOwnerSay("BWind now blowing from Southwest");
            }
            if (msg=="se") {
                windRose ="Southeast ";
                windDir=(315*DEG_TO_RAD);
                llOwnerSay("BWind now blowing from Southeast");
            }
            if (msg=="w") {
                windRose ="West ";
                windDir=(180*DEG_TO_RAD);;
                llOwnerSay("BWind now blowing from West");   
            }
            }
                
 
            // ----------------------------- END WIND DIRECTION PRESETS ---
            
            ///////////////////////////////////////////////////////////////
            // BWind Engine - BWIND SPEED PRESETS ////////////////////////
            //////////////////////////////////////////////////////////////
            
            //this is where you have to mod to set your desired BWIND Wind PRESETS
            // REMEBER THAT THE BOAT SPEED IS SET IN THE TRIM/HUD ROUTINE ABOVE - here you set the max speed only for each wind preset...            
            //8 KNOTS PRESET
            
            if (msg=="8") {
                //primary parameters - self explanatory
                windSpeed=4.12; //PRECALCULATED WIND SPEED                 
                maxSpeed=4.0;    //YOUR BOAT MAX SPEED FOR THIS WIND PRESET 
                heelTweak=0.7;   //YOUR BOAT MAX HEEL FOR THIS WIND PRESET              
                llOwnerSay("BWind speed set to 8 Knots");
                windType="8 Knots"; 
            }
            
            // 11 KNOTS PRESET
            
            if (msg=="11") {

                //primary parameters - see above for explanation (8 Knots Preset)
                windSpeed=5.70;
                maxSpeed=4.5;
                heelTweak=0.8;
                llOwnerSay("BWind speed set to 11 Knots");
                windType="11 Knots"; 
            }
            
            // 15 KNOTS PRESET (now takes initial settings)
            
            if (msg=="15") {
                
                //primary parameters - see above for explanation (8 Knots Preset)
                windSpeed=initwindSpeed;
                maxSpeed=initmaxSpeed;
                heelTweak=initheelTweak;
                llOwnerSay("BWind speed set to 15 Knots");
                windType="15 Knots";
            }
            
            // 18 KNOTS PRESET
            
            if (msg=="18") {
                
                //primary parameters - see above for explanation (8 Knots Preset)
                windSpeed=9.30;
                maxSpeed=6.5;
                heelTweak=0.95;
                llOwnerSay("BWind speed set to 18 Knots");
                windType="18 Knots"; 
            }
            
            // 21 KNOTS PRESET
            
            if (msg=="21") {

                //primary parameters - see above for explanation (8 Knots Preset)
                windSpeed=9.5;
                maxSpeed=7.0;
                heelTweak=1.0;
                llOwnerSay("BWind speed set to 21 Knots");
                windType="21 Knots"; 
            }
            
            // 25 KNOTS PRESET

            if (msg=="25") {

                //primary parameters - see above for explanation (8 Knots Preset)
                windSpeed=11.3;
                maxSpeed=7.5;
                heelTweak=1.1;
                llOwnerSay("BWind speed set to 25 Knots");
                windType="25 Knots"; 
            
            }        
            // ------------------------------ END WIND SPEED PRESETS ---
                       
            //////////////////////////////////////////////////////////////
            // UTILITIES ////////////////////////////////////////////////
            ////////////////////////////////////////////////////////////
            
            //Help Message
            //here you may build your own help message, shown when you type 'help' without quotes in chat...
            
            if (msg=="help") {
                helpString = " ";
                helpString+="SAY IN CHAT...\n";
                helpString+="------------------------------------------------------------------------------------------------\n";
                helpString+="raise - raise sails (up/down arrow to trim)\n";
                helpString+="lower - lower sails\n";
                helpString+="moor - stop sailing\n";
                helpString+="motor - lower sails, start engine";
                helpString+="hold - hold current heading\n";
                helpString+="setter - use OE/WeatherOS setter if present\n";
                helpString+="setoff - revert back to manual wind setting\n";
                helpString+="sheet nn - add nn to sheet\n";
                helpString+="trim - set autotrim mode\n";
                helpString+="tack - set autotack mode\n";
                helpString+="------------------------------------------------------------------------------------------------\n";
                helpString+="n,s,e,w,nw,ne,sw,se,setter,setoff - set BWind direction\n";
                helpString+="8,11,15,18,21,25 - set BWind speed\n";
                helpString+="id nnaa (ex. 12AB) - set boat ID\n";
                helpString+="hud - hud toggle (on/off)\n";
                helpString+="spin - hoist/drop Spinnaker (press PgUp & PgDn to trim)\n";
                helpString+="genoa - hoist/drop Genoa\n";
                helpString+="gybe - gybe Spinnaker's pole upon tack\n";
                helpString+="------------------------------------------------------------------------------------------------\n";
                helpString+="HUD colour shows trim settings :\n";
                helpString+="|>><<| red = too loose - |><| yellow = off optimum - \n";
                helpString+="|=| green = optimum - |<>| cyan = too tight\n";
                helpString+="------------------------------------------------------------------------------------------------\n";
                llOwnerSay(helpString);

            }
            
            // HUD Switch - toggle the HUD on or off. 
            
            else if (msg=="hud" && HUD_ON) HUD_ON=FALSE;
            else if (msg=="hud" && !HUD_ON) HUD_ON=TRUE;
                  
        }
        
        if (channel==-54001)                    // setter buoy, if present, is on this channel
        {
            
            if (useSetter==TRUE)                // don't do anything unless the setter is active
           
            {
                if (id!=W_id)       //is this a different buoy from the last one?
                {                   //only listen to it if it is closer than the last
                    vector npos=llList2Vector(llGetObjectDetails(id,[OBJECT_POS]),0)+llGetRegionCorner();
                    vector pos=llGetPos()+llGetRegionCorner();
                    if (llVecDist(pos,npos)>=llVecDist(pos,W_pos))  //this buoy farther away
                        return;                                     //then ignore it
                    W_id = id;              //closer, then remember it
                    W_pos = npos;
                }	//fall through to the code when it is the same wind buoy
                W_dog = 0.0;            //seconds since I last saw this wind setter.
                list lines=llParseString2List(msg,["\n"],[]);
            
                if (llList2String(lines,0)=="wind")             // look for a wind message
                {
                    list parse=llParseString2List(llList2String(lines,1),["=",";","/"],[]);
                    string var=llList2String(parse,0);
                    string val=llList2String(parse,1);
                    
                    if (var=="wvel")                            // take wind velocity as a vector
                    {
                        wind=(vector)val;
                        windSpeed = llVecMag(wind);              // m/s for the calculations
                        maxSpeed = windSpeed*0.75;              // generically, max speed is 3/4 the wind speed
                        windSpeed = windSpeed*1.94;             // change wind speed to knots
                        windDir = ((llAtan2(wind.x,wind.y)* RAD_TO_DEG)+180.0); // true direction
                        windType = llGetSubString((string)windSpeed,0,4) + " Knots";
                        // do the heel tweaks based on windspeed
                        if (windSpeed > 25.0) heelTweak = 1.2;
                        else if (windSpeed > 21.0) heelTweak = 1.1;
                        else if (windSpeed > 18.0) heelTweak = 1.0;
                        else if (windSpeed > 15.0) heelTweak = 0.95;
                        else if (windSpeed > 11.0) heelTweak = 0.85;
                        else if (windSpeed > 8.0) heelTweak = 0.8;
                        else heelTweak = 0.7;
                        
                         // now set the compass rose too
                         
                        if (windDir > 336.0) windRose = " North ";
                        else if (windDir > 290.0) windRose = " Northwest ";
                        else if (windDir > 250.0) windRose = " West ";
                        else if (windDir > 202) windRose = " Southwest ";
                        else if (windDir > 157.0) windRose = " South ";
                        else if (windDir > 112.0) windRose = " Southeast ";
                        else if (windDir > 67.0) windRose = " East ";
                        else if (windDir > 22.0) windRose = " Northeast ";
                        else windRose = " North ";

                        
                        windRose = "from " + llGetSubString((string)windDir,0,4) + "º " + windRose;
                    
                        windDir = (450.0 - windDir)%360.0;  // normalise compass
                        windDir = windDir*DEG_TO_RAD;       // this may be a bug. Investigate it
                }
            }
       } }
    }
    
    // --------------------------------------- END UTILITIES ---
    
    // GLOBAL BOAT CONTROLS 
    
    //Following section maps keyboard keys for boat control... Be careful editing these parameters, you break it, you own it.
 
    control(key id, integer held, integer change) {
        //turning controls - LEFT AND RIGHT KEYS
        if ( (change & held & CONTROL_LEFT) || (held & CONTROL_LEFT) || (change & held & CONTROL_ROT_LEFT) || (held & CONTROL_ROT_LEFT) ) {
            if (courseHold)
            {
                courseHold = FALSE;
                llOwnerSay("disengaging course hold...");
            }
            if (sailing == FALSE) llSetVehicleVectorParam(VEHICLE_ANGULAR_MOTOR_DIRECTION,<-rotSpeed/2.0,-0.20,llFabs(rotSpeed/2.5)>);
            else llSetVehicleVectorParam(VEHICLE_ANGULAR_MOTOR_DIRECTION,<-rotSpeed,-0.22,rotSpeed/2.5>); // left key hold - end
        }
        else if ( (change & held & CONTROL_RIGHT) || (held & CONTROL_RIGHT) || (change & held & CONTROL_ROT_RIGHT) || (held & CONTROL_ROT_RIGHT) ) {
    
            if (courseHold)
            {
                courseHold = FALSE;
                llOwnerSay("disengaging course hold...");
            }
            
            if (sailing == FALSE) llSetVehicleVectorParam(VEHICLE_ANGULAR_MOTOR_DIRECTION,<rotSpeed/2.0,-0.20,-rotSpeed/2.5>);
            else llSetVehicleVectorParam(VEHICLE_ANGULAR_MOTOR_DIRECTION,<rotSpeed,-0.22,-rotSpeed/2.5>); // right key hold - end
        }
        else if ( (change & ~held & CONTROL_LEFT) || (change & ~held & CONTROL_ROT_LEFT) ) {
            llSetVehicleVectorParam(VEHICLE_ANGULAR_MOTOR_DIRECTION,<0.0,0.0,0.0>); // left key touched - end
        }
        else if ( (change & ~held & CONTROL_RIGHT) || (change & ~held & CONTROL_ROT_RIGHT) ) {
            llSetVehicleVectorParam(VEHICLE_ANGULAR_MOTOR_DIRECTION,<0.0,0.0,0.0>); // right key touched - end
        } 
        
        //sail/throttle controls - UP AND DOWN KEYS
        // 1-41-OS now check if we're sailing or in motor mode it makes a difference
       
        
        if ( (held & CONTROL_FWD) && (held & CONTROL_UP) ) {
            if (sailing == TRUE) {
                sheetAngle+=7;
                if (sheetAngle>maxAngle) sheetAngle=maxAngle; 
                if (autoTrim)
                {
                    autoTrim = FALSE;
                    llOwnerSay("disengaging auto trim.");
                }
            }
            else
            {
                if (motorOn)
                {
                    if (throttle < throttleMax)
                    {
                        if (!klimit)
                        {
                            throttle += 1;
                            dispThrot = (integer)throttle;
                            llOwnerSay("throttle "+(string)dispThrot);
                            klimit = TRUE;               // don't flood the throttle 
                        }
                    }
                    else 
                    {
                        llSetVehicleVectorParam(VEHICLE_LINEAR_MOTOR_DIRECTION,<5.0,0,0>);      // with sail down and motor off, you can still move, it's just not realistic.
                    }
                }
            }
        } // up key hold - end
        else if ( (held & CONTROL_FWD) || (change & held & CONTROL_FWD) ) {
            if (sailing == TRUE) {
                sheetAngle+=2;
                if (sheetAngle>maxAngle) sheetAngle=maxAngle; 
                if (autoTrim)
                {
                    autoTrim = FALSE;
                    llOwnerSay("disengaging auto trim.");
                } 
            } else {
                if (motorOn)
                {
                    if (throttle < throttleMax)
                    {
                        if (!klimit)
                        {
                            throttle += 1;
                            dispThrot = (integer)throttle;
                            llOwnerSay("throttle "+(string)dispThrot);
                            klimit = TRUE;
                        }
                    }
                    else 
                    {
                        llSetVehicleVectorParam(VEHICLE_LINEAR_MOTOR_DIRECTION,<5.0,0,0>);
                    }
                } 
            }                             
        } // up key touched - end
        else if ( (held & CONTROL_BACK) && (held & CONTROL_UP) ) {
            
            if (sailing == TRUE) {
                sheetAngle-=7;
                if (sheetAngle<5) sheetAngle=5;
                if (autoTrim)
                {
                    autoTrim = FALSE;
                    llOwnerSay("disengaging auto trim.");
                }      
            }
            else
               if (motorOn)
                {
                    if (throttle > -throttleMax)
                    {
                        if (!klimit)
                        {
                            throttle -= 1;
                            dispThrot = (integer)throttle;
                            llOwnerSay("throttle "+(string)dispThrot);
                            klimit = TRUE;
                        }
                    }
                    else 
                    {
                        llSetVehicleVectorParam(VEHICLE_LINEAR_MOTOR_DIRECTION,<5.0,0,0>);
                    }
                }
        } // down key hold - end
        else if ( (held & CONTROL_BACK) || (change & held & CONTROL_BACK) ) {
            if (sailing == TRUE) {
                sheetAngle-=2;
                if (sheetAngle<5) sheetAngle=5;
                if (autoTrim)
                {
                    autoTrim = FALSE;
                    llOwnerSay("disengaging auto trim.");
                }         
            } else {
               if (motorOn)
                {
                    if (throttle > -throttleMax)
                    {
                        if (!klimit)
                        {
                            throttle -= 1;
                            dispThrot = (integer)throttle;
                            llOwnerSay("throttle "+(string)dispThrot);
                            klimit = TRUE;
                        }
                    }
                    else 
                    {
                        llSetVehicleVectorParam(VEHICLE_LINEAR_MOTOR_DIRECTION,<5.0,0,0>);
                    }
                }
            }
        } // down key touched - end

         //PGUP/PGDN Spinnaker control
        else if (change & held & CONTROL_UP) {
            TrimSpinPlus();
        }
        else if (change & held & CONTROL_DOWN) {
            TrimSpinMinus();
        }
        
        if (!held)                      // you stopped touching the key
        {
                    klimit = FALSE;                 // you may now press again
        }    
    }    
    
    // ------------------------------------- END GLOBAL BOAT CONTROLS ---

    // (ignore link messages to the root prim)
    link_message(integer from,integer to,string msg,key id) {
    }
    
    ///////////////////////////////////////////////////
    // GLOBAL BOAT TIMER /////////////////////////////
    // you don't want to modify these settings //////
    ////////////////////////////////////////////////
    
    //IMPORTANT !!! THIS IS THE ACTUAL BOAT CYCLE INVOKING ALL SAILING ROUTINES
    //WHATEVER YOU DELETE HERE WILL AFFECT ACTUAL ENGINE BEHAVIOUR
    //HERE YOU MAY ADD YOUR OWN PERSONAL FUNCTIONS IF NEEDED AND YOU KNOW WHAT TO DO...

    
    timer() {

        if ((W_dog+=timerFreq) > 15.0)      //if I have not heard this Wind Setter for 15 seconds
        {
            W_id = NULL_KEY;            //forget it so you will switch to next one
            W_pos = <3000,3000,3000>;
        }
        calcTrueWindAngle();
        calcAppWindAngle(); // invoke wind angle calculation routine
        if (SAIL_UP) calcBoomDelta(); // invoke sail trim calculation routine
        calcHeelAngle(); // invoke heel calculation routine
        calcSpeed(); // invoke speed calculation routine
        calcLeeway(); // invoke leeway calculation routine
        calcTurnRate(); // invoke turn rate routine
        updateHUD(); // update hud on cycle
        
        if (motorOn)                // if we're in motor mode, use the engine, otherwise use the sail mode
        {
            motorRun(); // if motor is on, use it
        }
        else
        {
            llSetVehicleVectorParam(VEHICLE_LINEAR_MOTOR_DIRECTION,<currSpeed,leeway,heelHeight>); // boat linear movement
            llSetVehicleVectorParam(VEHICLE_ANGULAR_MOTOR_DIRECTION,<heelTorque,0.0,0.0>); // boat angular movement
        }
        if (courseHold) updateCourse(); // if AP is on, adjust course
        if (autoTack) checkTack();
        setWindvane(windDir*RAD_TO_DEG);
        if (useSetter) setINS1(windSpeed,windDir*RAD_TO_DEG);
        else setINS1(windSpeed*1.944,windDir*RAD_TO_DEG);
    }
    
    // -------------------------------------- END GLOBAL BOAT TIMER ---
   
}

// ------------------------------------- END PROGRAMME -----------------------------------------------------------------------