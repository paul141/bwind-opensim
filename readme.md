 EdisonRex July 2020

 BWIND IS GPL 
 ------------
 
 Forks of this scriptset are welcome. Cooperation is even more welcome. 

 Using AVSitter with bwind:
      You do not need to use the bbk_pose script if you use AVSitter on your boat. In fact, using AVSitter is much better. If you want to see examples, drop by.
      I do not recommend the use of PMAC with bwind or any boats for that matter. There are far too many technical issues especially around OSSL function
      permissions. 

 CHANGE HISTORY
---------------------

 Version 1-43-OS-ME (Opensim Mesh Support Motor Edition) adds or fixes the following:

 1) Enhancement to the heeling to take into account hull width and raise the centre of the hull (technically this is raising the centre of the hull by 1/2 the height of the right triangle described by the heel angle and the hull width). This is especially useful for catamarans which shouldn't have their pontoons underwater when they heel. It's also useful for any larger hulled vessel which is taking on lots of water when you heel, it just is a bit more natural.
 2) Kayaker Magic was kind enough to add the code for his setter buoys which select the closest of the buoys, I had not included his code originally, many thanks to Kayaker for the fix. The Ocean Engineering wind buoys have been supported in bwind Opensim Edition for a couple of years, you should definitely get one as they are great for making consistent wind on larger oceans. The WeatherOS buoys use the same standard and make real life wind conditions available in Opensim. Either one is supported (or both!)
 3) Setting the height of the keel in the water is now done in one place, and the boat will adjust height to that offset. So many different mesh boats have high or low hulls that you shouldn't have to go find in the code where to set this. 
 4) A few bugfixes due to increased use of YEngine which is more strict about lsl syntax, and takes divide-by-zero seriously. In general this script system works best on ubODE physics with either XEngine or YEngine. 

 Version 1-42-OS-ME (Opensim Motor Edition) adds the following:

 1) Auto Sail Trim - type "trim" with the sails up and they will trim themselves. Change your
     course and the sails will trim automatically. Touch the sheet (up/down arrow) to disengage.
 2) Auto Tack - Set yourself on your first tack course (or gybe course if downwind) and type "tack"
   to have the boat automatically set the next tack (or gybe) leg course. The boat will steer itself 
  onto the new course and the sails will adjust themselves. Type "tack" for each
  following change of tack. 

 3) additional instruments supported: Sail Trim Indicator and Combination Wind Speed
    and Direction. The STI is documented separately and the Combination WS/WD is just to accommodate where to send messages to.
    Ultimately given how many link messages float around the boat, the code would ultimately be simpler if child links were better at managing messages.

 4) Some additional bugs/inconsistencies fixed.
    - initial 15 knot wind speed does not need to be set twice. Set it once where indicated.
    - hud is now a toggle on/off. Many boats now have onboard instruments, and you can wear HUDs if you want.
    - help text has been updated to reflect all these changes
    - Fenders will appear when mooring coming from motor mode.
    - lots of comments rephrased or removed, if you care to read comments you probably know enough about what you want to do.
    - Almost all interactions with the script now use llOwnerSay instead of llWhisper, as passengers don't need the spam. At some point the whole
          guest pilot design needs to be re-examined and then these messages may just speak to the pilot as opposed to the owner. But not at this time.
    - The y-axis on the angular motor is in need of some proportion. In the meantime, tweak it to try to keep the boats from nosing in (or dragging their sterns).
         Larger boats have it worse than smaller boats but in general Opensim boats are larger so it needs to be addressed.


 Version 1-41-OS-ME (Opensim Motor Edition) adds the following:

 1) Motor mode. Instead of having to constantly press the up arrow or down arrow key with sails down to move, the command "motor" will:
      - bring sails down if up, will enable physics if not. Either way the boat will go physical and sails are forced down
      - changes up and down arrow to throttle mode, meaning single press increments or decrements the throttle.
      - the pbsEngine script can be used to manage engine sounds (if you haven't seen Power Boat System, you will now)
      - engine messages for tachometer, water temp, oil pressure, fuel are supported. Fuel will deplete over time. Replenish by re-sitting.
      - directional indication is supported as well. This is mostly an adaptation of PBSLite 1.0.
      - PBS accessories supported are pbsPropeller, pbsSmoke, the gauges for oil, water, fuel and tach, and pbsEngine.

 2) Building on the mesh sail support in 1-40-OS, the scripts are tolerant of the additional motor messages - granted, they should be down when the motor
          is running but they still needed to know about the extra messages running through the boat in motor mode. Because link messages aren't directed to 
          link members in this case they'll hear engine messages every timer tick. The so-called sail alpha adjustment which doesn't really work well is deprecated in this
          branch, which implicitly fixes the motor message pseudo-issue. 

 3) Fix the help command to describe all of the changes since 1-37
 4) change this document to show changes in reverse order. Update the distribution box with new scripts. Add the accessories for PBS to another box
          for use in this scriptset. Add a scripting guide for all of this. 

 Version 1-40-OS (Opensim) adds the following:

 1) Mesh Sail Support. The sail code has been drastically rewritten too. Mesh sails don't need to have their pivots on the edge of the sail, you can 
 specify where the pivot point is, and the script will figure it out. This works for the boom, the jib, and any new mesh sails.
 2) New sail type Genoa. Type "gen" to deploy. It is sort of like a jibby spinnaker. You can't have the spinnaker out and the genoa at the same time. 
 there is a new sail script for "Genoa" to use. 
 3) Mesh sails can flap. This adds flap support in the main code to send to the sails and other things that want to know. The new message is 1010 (I didn't pick it, but
 it makes sense to use it). If you use this, all the scripts have to hear 1010 messages and process them otherwise things spin around during flapping time. 
 4) Flap messages come from bwind, for red and yellow modes you will get flap. The sails process that message and add sounds etc.
 5) Previous version changes (there are a lot!) will now go into a notecard to go in the kit (BBK_CHANGES-1-xx-OS-ME) because this is taking up too much script space.


 Version 1-39-OS (Opensim) adds the following new functions:
 
 1) Implement course hold command. Type "hold" and the boat will follow the
 current course/heading (in other words, the boat will stay pointed
 in the direction you are heading, but that will also hold your course).
 If you touch the wheel (left or right keys) it will disengage. So there is no "unhold" command for that reason.
 2) Some support for linked gauges is introduced here too. Specifically AWA is
 now directly supported as well as a tachometer, the tach is rudimentary because
 the engine is rudimentary. 
 3) A number of bugs are fixed here. The most noticeable was the spinnaker appearing on sit, the startup and setup is complicated for
 less than good reasons. quite a few other bugs in the hud, and other places.
 4) not directly part of this script, but spinnaker will shrink when not in use to prevent collisions, and the wake uses 1/2 the particles
 and 2/3 the life to save on frame rate.
 5) Turn the tacking animation support on or off with a single flag. This saves commenting out code or remembering to.
 6) AWI and TWI to gauges support
 7) SAI (Sail Angle Indicator) gauge support added, this means sail angle gets sent +- to the gauge


 Version 1-38-OS (Opensim) adds the following:

 1) Spinnaker (saw this in SL so it isn't mine)
 2) Spinnaker Pole (with the spinnaker)
 3) Fenders, hatch, tackle  -  for boats equipped with fenders or hatches or tackle that you want to either disappear or 
 appear depending on if you are sailing or not, there's support for this, name the fender prim "fender" and optionally the 
 face on the mesh prim. Likewise for hatch, also if the boom tackle is out, you can make that disappear too.

 4) Ocean Engineering/WeatherOS wind setter to BWind (new commands setter/setoff) this takes real life wind 
 via the setter buoys, or any Ocean Engineering setter buoy for that matter, giving you "race mode" but maybe even 
 a little better even when not racing.

 5) Windvane follows wind setting (stop using that stupid sim wind) - yes, a new windvane added. There is also a windvane script
 you need to install in the windvane. The windvane will point into the direction bwind is set to, and will stay pointed at that direction.

 6) tweaks for ubODE - mostly around the X-axis which seems determined to nose into the water on turns.
 7) disable motor when sails are up. That seemed like a bug. 
 
 You can adapt this to any boat, if you wish. It would be nicer with mesh
 sails, but the centres of the prims have to be on the edge to work right, you were warned. See the documentation.
 (nb Mesh Sails are supported now in 1-40-OS)
