////////////////////////////////////////////////////////
// BWind_SPINNAKER_1-42-OS-MeshC /////
// BWind Boats - SPINNAKER script     /////
// Becca Moulliez - March 2010          /////
// modified EdisonRex March 2019 GPL
////////////////////////////////////////////////////

//------------------------------------------------------
//            Please do not remove this header
//                Permitted Free Uses
//  - allowed to use in your personal boats. 
//  - allowed to use in boats you wish to sell or give away in second life.
//  - allowed to modify any part to your particular needs
// ======= End Header =========
//
// this modification shrinks the spinnaker when lowering, also use llSetLinkPrimitiveParamsFast instead.
// of course raising the spin will grow it to the size you want it. You need to set that size though for each spinnaker.
// now with rotation and position setting like the jib. 1-41 let's support mesh in a meaningful way. 1-42 fixes all the bugs in 1-41.


rotation childRot;
rotation quatRot;
rotation initRot;
vector eulerRot;
vector initPos;
integer sailUp;         // support mesh faces


// to flap the spinnaker, you have to actually flap both left and right sides.
// in this particular case we got 4 faces. 
flapspin()
{
    if (sailUp)
    {
    integer i;
    integer j;
    for (i=0;i<5;i++)
        {
            if (j==1)
            {
                llSetAlpha(0.0,0);      // 
                llSetAlpha(1.0,1);
                llSetAlpha(0.0,2);
                llSetAlpha(1.0,3);
                llSetAlpha(0.0,4);
                j = 0;
            }
            else
            {
                llSetAlpha(1.0,0);      // 
                llSetAlpha(0.0,1);
                llSetAlpha(0.0,2);
                llSetAlpha(0.0,3);
                llSetAlpha(1.0,4);
                j = 1;
            } 
            llSleep(0.15);              
               
        }
    }
}
            

lower() {
    llSetLinkPrimitiveParamsFast(LINK_THIS, [ PRIM_COLOR, ALL_SIDES, <1,1,1>, 0.0 ]);
    llSetLinkPrimitiveParamsFast(LINK_THIS, [ PRIM_SIZE, <1,1,1>]);
    sailUp = FALSE;
}

raise () {
 
      llSetLinkPrimitiveParamsFast(LINK_THIS, [ PRIM_COLOR, ALL_SIDES, <1,1,1>, 0.0 ]); // we'll raise down below
      llSetLinkPrimitiveParamsFast(LINK_THIS, [ PRIM_SIZE, <14.0,13.5,50.0>]);
      sailUp = TRUE;
}

reset() {
    llSetLinkPrimitiveParamsFast(LINK_THIS, [ PRIM_ROT_LOCAL, initRot ]);
    llSetLinkPrimitiveParamsFast(LINK_THIS, [ PRIM_COLOR, ALL_SIDES, <1,1,1>, 0.0 ]);
    llSetLinkPrimitiveParamsFast(LINK_THIS, [ PRIM_SIZE, <1,1,1>]);
}

default {
    
    state_entry() {
        llSetObjectName("spinnaker");              //SET HERE SAIL PRIM NAME
        initRot=llEuler2Rot(<0,-24.0*DEG_TO_RAD,0*DEG_TO_RAD>); //DOES NOT ROTATE FRONTALLY - IF NEEDED CHECK JIB SCRIPT
        llSetLinkPrimitiveParamsFast(LINK_THIS, [ PRIM_ROT_LOCAL, initRot ]);
        llSetStatus(STATUS_PHANTOM,FALSE); 
        llSetLinkPrimitiveParamsFast(LINK_THIS, [ PRIM_COLOR, ALL_SIDES, <1,1,1>, 0.0 ]);      
    }
    
    on_rez(integer param) {
        llAllowInventoryDrop(TRUE);
    }
    
    link_message(integer sender,integer num,string str,key id) {
       
        if (num==1000) {        //drop with reset
            lower();
            reset();
        }
        else if (num==1001) {   //drop without reset
            lower();
        }
        else if (num==1002) {   //hoist spinnaker
            raise();
        }
        else if (num==1003) {   //reset sail
            reset();
        }
        else if (num==1004) {   //reset sail
            raise();
            reset();
        }
        else if (num==1010)
        {
            if (str == "flap") flapspin();         // flap the spinnaker
            else if (sailUp)
            {
                llSetAlpha(1.0,0);      // 
                llSetAlpha(0.0,1);
                llSetAlpha(0.0,2);
                llSetAlpha(0.0,3);
                llSetAlpha(1.0,4);
            }
            else
            {
                llSetAlpha(0.0,0);      // 
                llSetAlpha(0.0,1);
                llSetAlpha(0.0,2);
                llSetAlpha(0.0,3);
                llSetAlpha(0.0,4);
            }
        }
        else if (num>1999) {    //no, don't mess with alphas now
            // do nothing
            
        }   
        else {                  //rotate sail, CCW is positive
            childRot=llGetLocalRot();
            eulerRot=<0,0,num*DEG_TO_RAD>;
            quatRot=llEuler2Rot(eulerRot);
            llSetLinkPrimitiveParamsFast(LINK_THIS, [ PRIM_ROT_LOCAL, quatRot*childRot ]);
            
            if (sailUp)
            {
                llSetAlpha(1.0,0);      // 
                llSetAlpha(0.0,1);
                llSetAlpha(0.0,2);
                llSetAlpha(0.0,3);
                llSetAlpha(1.0,4);
            }

        }     
    }    
}